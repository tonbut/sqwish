![Sqwish](doc/img/sqwish-logo.png "Sqwish")

**Sqwish** is a compute cluster technology. It enables many discrete computers to be connected into a cluster to provide high performance and fault tolerance. In contrast to technologies such as Kubernetes it's driving aim is to be simple - simple to understand, use, and deploy.

## Concepts

Sqwish consists of three types of instance, which may or may not be combined into a single physical host depending upon environment. For example for developing it might be easier to have all instance types running on one laptop. In a real world deployment it obviously only has benefit if there are multiple agents running on separate physical hosts to gain the scaling and fault tolerance.

The cluster manager is the central node for receiving and dispatching requests. The cluster manager runs a secure server that allows agents and clients to connect. The cluster manager encrypts all communications over TLS and can be configured with a certificate so that agents and clients can verify its identity. Agents and clients must provide authentication details to the cluster manager so that they can be verified. The cluster manager supports a dashboard to allow monitoring and control.

An agent is a compute instance that connects to the cluster manager. It's purpose is to accept requests from the cluster manager and process them. In order to process requests it must receive functionality and install and keep it up to date. The cluster manager tells agents what functionality to install and the agent tells the cluster manager when it has successfully installed it. In addition the agents report telemetry data to the cluster manager. Agents can be added and removed, either cleanly or hard, without loss of requests.

Clients connect to the cluster manager to issue requests. Clients requests are sent to the cluster manager which attempts to route the request to an agent. The response is the returned to the client. Requests are subject to configurable timeouts and retry limits. Agents may act as clients, this allows the creation of a service mesh.

Namespaces are logical groupings of functionality that define the deployment unit onto agents and the available functionality interface to clients. Namespaces consist of set of NetKernel modules which will be installed into agents. 

## Build
1. Clone this repository
2. Open a terminal in the root directory of the repository and run the command `./gradlew build.`

## Installation

See the [Installation Guide](doc/installation.md).

## Configuration
See the [Configuration Guide](doc/configuration.md).

## Running the example(s)
See the [Example Guide](doc/example.md).

## Dashboard
See the [Dashboard Guide](doc/dashboard.md).

## Developing with Sqwish
See the [Development Guide](doc/development.md).

## Roadmap
See the [Roadmap](doc/roadmap.md).

## Internal Developer Reference
This [page](doc/resourceGuide.md) shows the structure of the internal resources and data strutures.

