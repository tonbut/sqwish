package io.sqwish.client.test;

import java.util.concurrent.atomic.AtomicLong;

import org.netkernel.layer0.nkf.INKFAsyncRequestListener;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.util.Utils;

public class DataSetTest extends StandardAccessorImpl
{
	public DataSetTest()
	{
		this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		float avgLatency=-1;
		float F=0.1f;
		
		long DURATION=120000L;
		int DATASET_SIZE=500;
		long period=10L;
		long start=System.currentTimeMillis();
		long now;
		
		long issued=0;
		AtomicLong received=new AtomicLong();
		AtomicLong totalDuration=new AtomicLong();
		int lastI=0;
		do
		{
			now=System.currentTimeMillis();
			
			if (now-start<DURATION)
			{
			
				String identifier="res:/modulec/caching/"+Integer.toString((int)(Math.random()*DATASET_SIZE));
				INKFRequest req=aContext.createRequest(identifier);
				final long reqStart=now;
				issued++;
				aContext.issueAsyncRequest(req).setListener(new INKFAsyncRequestListener()
				{
					
					@Override
					public void receiveResponse(INKFResponseReadOnly aResponse, INKFRequestContext aContext) throws Exception
					{
						
						totalDuration.addAndGet(System.currentTimeMillis()-reqStart);
						received.incrementAndGet();
						aContext.setNoResponse();
						
					}
					
					@Override
					public void receiveException(NKFException aException, INKFRequest aRequest, INKFRequestContext aContext)
							throws Exception
					{
						System.out.println("error");
						totalDuration.addAndGet(System.currentTimeMillis()-reqStart);
						received.incrementAndGet();
						aContext.setNoResponse();
					}
				});
			}
			
			int i=(int)((now-start)/500);
			if (i!=lastI)
			{	
				lastI=i;
				long receivedTime=received.get();
				long responseTime=receivedTime>0?totalDuration.get()/receivedTime:0;
				System.out.println(i+" "+period+" "+issued+"->"+received+" "+responseTime);
				
				period=(long)(2+(1-Math.cos(((float)i)/20))*35);
			}
			
			Thread.sleep(period);
			
			
		} while(now-start<DURATION || issued>received.get());
		
		
		
	}
	
}
