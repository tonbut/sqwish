package io.sqwish.client.test;

import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.util.Utils;

public class SlowTest extends StandardAccessorImpl
{
	public SlowTest()
	{
		this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		float avgLatency=-1;
		float F=0.05f;
		for (int i=0; i<1000; i++)
		{
			long t0=System.currentTimeMillis();
			String result=aContext.source("res:/modulec/java",String.class);
			t0=System.currentTimeMillis()-t0;
			if (avgLatency<0)
			{	avgLatency=(float)t0;
			}
			else
			{	avgLatency=avgLatency*(1-F)+((float)t0)*F;
			}
			String instance=result.substring(0,result.indexOf(' '));
			System.out.println("step "+(i+1)+" "+instance+" "+t0+" "+String.format("%.1f", avgLatency));
			Thread.sleep(10);
		}
	}
}
