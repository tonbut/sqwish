package io.sqwish.client.test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.netkernel.layer0.nkf.INKFAsyncRequestHandle;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.util.Utils;

public class BlastTest extends StandardAccessorImpl
{
	public BlastTest()
	{
		this.declareThreadSafe();
	}
	
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		int N=500;
		INKFAsyncRequestHandle[] handles=new INKFAsyncRequestHandle[N];
		for (int i=0; i<N; i++)
		{
			//String uri=(i%2==0)?"res:/modulec/java":"res:/modulec/groovy";
			String uri="res:/modulec/java";
			INKFRequest req=aContext.createRequest(uri);
			req.setRepresentationClass(String.class);
			handles[i]=aContext.issueAsyncRequest(req);
		}
		
		int waitingFor=N;
		int last=waitingFor;
		Map<String,AtomicInteger> counts=new HashMap<String, AtomicInteger>();
		while(waitingFor>0)
		{
			for (int i=0; i<N; i++)
			{	INKFAsyncRequestHandle handle=handles[i];
				if (handle!=null)
				{
					INKFResponseReadOnly response=handle.joinForResponse(1);
					if (response!=null)
					{	waitingFor--;
						handles[i]=null;
						String rep=(String)response.getRepresentation();
						if (rep!=null)
						{
							String instance=rep.substring(0,rep.indexOf(' '));
							AtomicInteger c=counts.get(instance);
							if (c==null)
							{	c=new AtomicInteger(1);
								counts.put(instance,c);
							}
							else
							{	c.incrementAndGet();
							}
						}
						else
						{	System.out.println("got null");
						}
						//System.out.println("rep="+rep);
						/*
						if (rep!=null)
						{
							System.out.println("rep="+rep);
						}
						*/
					}
				}
				
			}
			if (waitingFor!=last)
			{	System.out.println("waiting for "+waitingFor+" responses "+dump(counts));
			}
			last=waitingFor;
			Thread.sleep(200);
		}
		System.out.println("waiting for nothing - test complete "+dump(counts));
			
	}
	
	private String dump(Map<String,AtomicInteger> counts)
	{
		StringBuilder sb=new StringBuilder();
		for (Map.Entry<String,AtomicInteger> e : counts.entrySet())
		{
			sb.append(e.getKey()+":"+e.getValue().get()+" ");
		}
		return sb.toString();
	}
}
