package io.sqwish.client.test;

import java.util.concurrent.Semaphore;

import org.netkernel.layer0.nkf.INKFAsyncRequestHandle;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.util.Utils;

public class ThroughputTest extends StandardAccessorImpl
{
	private Semaphore mSema=new Semaphore(1);
	public ThroughputTest()
	{
		this.declareThreadSafe();
	}
	
	private String issueRequest(String aIdentifier, INKFRequestContext aContext) throws Exception
	{
		
		INKFAsyncRequestHandle h;
		INKFRequest req=aContext.createRequest(aIdentifier);
		req.setRepresentationClass(String.class);
		mSema.acquire();
		try
		{	h=aContext.issueAsyncRequest(req);
		}
		finally
		{	mSema.release();
		}
		return (String)h.join();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		for (int i=0; i<20; i++)
		{
			doStuff(aContext);
		}
	}
	
	private void doStuff(INKFRequestContext aContext) throws Exception
	{
		int N=32;
		Runner[] runner=new Runner[N];
		for (int i=0; i<N; i++)
		{
			Runner r=new Runner(aContext);
			r.start();
			runner[i]=r;
		}
		int c=0;
		for (int i=0; i<N; i++)
		{
			runner[i].join();
			c+=runner[i].getCount();
		}
		float rate=((float)c)/5.0f;
		System.out.println(rate+" req/sec");
	}
	
	private class Runner extends Thread
	{
		private final INKFRequestContext mContext;
		private int mCount;
		
		public Runner(INKFRequestContext aContext) { mContext=aContext; }
		public int getCount() { return mCount; }
		public void run()
		{
			try
			{
				long start=System.currentTimeMillis();
				do
				{
					String result=issueRequest("res:/modulec/java",mContext);
					mCount++;
				} while(System.currentTimeMillis()-start<5000L);
			}
			catch (Exception e)
			{	System.out.println(Utils.throwableToString(e));
			}
		}
		
	}
}
