package modulec;

import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class DummyAccessor extends StandardAccessorImpl
{
	public DummyAccessor()
	{
		this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		int l=0;
		int e=Integer.MAX_VALUE/8;
		for (int i=0; i<e; i++)
		{	l+=i;
		}
		String instanceId = aContext.source("netkernel:/config/netkernel.instance.identifier", String.class);
		INKFResponse resp=aContext.createResponseFrom(instanceId+" "+Integer.toString(l)+" "+System.currentTimeMillis());
		resp.setExpiry(INKFResponse.EXPIRY_ALWAYS);
	}
}