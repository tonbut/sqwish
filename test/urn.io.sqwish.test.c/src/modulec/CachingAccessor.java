package modulec;

import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class CachingAccessor extends StandardAccessorImpl
{
	public CachingAccessor()
	{
		this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String id=aContext.getThisRequest().getArgumentValue("id");
		int l=0;
		int e=Integer.MAX_VALUE/16;
		for (int i=0; i<e; i++)
		{	l+=i;
		}
		String instanceId = aContext.source("netkernel:/config/netkernel.instance.identifier", String.class);
		INKFResponse resp=aContext.createResponseFrom(instanceId+" "+id+" "+l+" "+System.currentTimeMillis());
		resp.setExpiry(INKFResponse.EXPIRY_CONSTANT, System.currentTimeMillis()+100);
	}
}