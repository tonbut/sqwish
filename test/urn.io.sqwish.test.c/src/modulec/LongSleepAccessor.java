package modulec;

import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class LongSleepAccessor extends StandardAccessorImpl
{
	public LongSleepAccessor()
	{
		this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		Thread.sleep(1000L);
		INKFResponse resp=aContext.createResponseFrom("done");
		resp.setExpiry(INKFResponse.EXPIRY_ALWAYS);
	}
}