package modulec;

import java.util.concurrent.atomic.AtomicReference;

import org.netkernel.layer0.nkf.INKFAsyncRequestListener;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class Fibonacci extends StandardAccessorImpl
{
	public Fibonacci()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		int n=Integer.parseInt(aContext.getThisRequest().getArgumentValue("n"));
		if (n<2)
		{	aContext.createResponseFrom(n);
		}
		else
		{
			INKFRequest req1=aContext.createRequest(String.format("fib(%d)", n-1));
			req1.setRepresentationClass(Integer.class);
			INKFRequest req2=aContext.createRequest(String.format("fib(%d)", n-2));
			req2.setRepresentationClass(Integer.class);
			
			INKFAsyncRequestListener l=new INKFAsyncRequestListener()
			{
				private AtomicReference<Integer> mFirstValue=new AtomicReference<>();
						
				@Override
				public void receiveResponse(INKFResponseReadOnly aResponse, INKFRequestContext aContext) throws Exception
				{
					Integer rep=(Integer)aResponse.getRepresentation();
					if (!mFirstValue.compareAndSet(null, rep))
					{
						int sum=rep+mFirstValue.get();
						aContext.createResponseFrom(sum);
					}
					else
					{	aContext.setNoResponse();
					}
				}
				
				@Override
				public void receiveException(NKFException aException, INKFRequest aRequest, INKFRequestContext aContext)
						throws Exception
				{
					throw aException;				
				}
			};
			
			aContext.issueAsyncRequest(req1).setListener(l);
			aContext.issueAsyncRequest(req2).setListener(l);
			aContext.setNoResponse();
		}
			
	}
}
