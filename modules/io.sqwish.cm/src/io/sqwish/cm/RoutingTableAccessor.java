package io.sqwish.cm;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;

import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.layer0.urii.ParsedIdentifierImpl;
import org.netkernel.layer0.util.GoldenThreadExpiryFunction;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class RoutingTableAccessor extends StandardAccessorImpl
{
	private Semaphore mSema=new Semaphore(1);
	private AtomicReference<GoldenThreadExpiryFunction> mExpiry=new AtomicReference<>();
	
	public RoutingTableAccessor()
	{	this.declareThreadSafe();
		mExpiry.set(new GoldenThreadExpiryFunction("RoutingTable"));
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String type=aContext.getThisRequest().getArgumentValue(ParsedIdentifierImpl.ARG_ACTIVE_TYPE);
		if (type.equals("routingTable"))
		{	//only allow one request at a time to ensure concurrent requests pull from cache
			mSema.acquire();
			try
			{
				Object result=aContext.source("active:routingTableInner");
				aContext.createResponseFrom(result);
			}
			finally
			{	mSema.release();
			}
		}
		else if (type.equals("routingTableInner"))
		{
			IHDSDocument result=innerSource(aContext);
			INKFResponse resp=aContext.createResponseFrom(result);
			resp.setExpiry(INKFResponse.EXPIRY_MIN_FUNCTION_DEPENDENT, mExpiry.get());
		}
		else if (type.equals("routingTableInvalidate"))
		{
			GoldenThreadExpiryFunction newExpiry=new GoldenThreadExpiryFunction("RoutingTable");
			GoldenThreadExpiryFunction lastExpiry=mExpiry.getAndSet(newExpiry);
			lastExpiry.invalidate();
		}
	}
	
	private IHDSDocument innerSource(INKFRequestContext aContext) throws Exception
	{
		IHDSReader assignments=aContext.source("active:agentAssignments",IHDSDocument.class).getReader();
		IHDSReader deploymentState=aContext.source("active:agentDeploymentState",IHDSDocument.class).getReader();
		IHDSReader agentList=aContext.source("active:agentList",IHDSDocument.class).getReader();
		
		INKFRequest req=aContext.createRequest("active:agentState");
		req.setRepresentationClass(IHDSDocument.class);
		req.setHeader(INKFRequest.HEADER_EXCLUDE_DEPENDENCIES, Boolean.TRUE);
		IHDSReader agentState=((IHDSDocument)aContext.issueRequest(req)).getReader();
		
		
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("namespaces");
		for (IHDSReader namespace : assignments.getNodes("/assignments/namespaces/namespace"))
		{	String namespaceId=(String)namespace.getFirstValue("@id");
			m.pushNode("namespace").addNode("id", namespaceId);
			for (Object agentIdRaw : namespace.getValues("agent"))
			{	String agentId=(String)agentIdRaw;
				if (isAgentGoodForRouting(agentId,deploymentState))
				{	
					IHDSReader agentListAgent=agentList.getFirstNodeOrNull("key('byId','"+agentId+"')");
					INKFRequestContext agentContext=(INKFRequestContext)agentListAgent.getFirstValueOrNull("context");
					if (agentContext!=null)
					{
						Long lastConnect=(Long)agentListAgent.getFirstValue("lastConnect");
						
						String cores=(String)agentState.getFirstValueOrNull("key('byId','"+agentId+"')/info/cpu/processors");
						if (cores==null)
						{	cores="1";
						}
						
						m.pushNode("agent")
						.addNode("id", agentId)
						.addNode("context", agentContext)
						.addNode("lastConnect",lastConnect)
						.addNode("cores",cores)
						.popNode();
					}
				}
			}
			m.popNode();
		}
		m.declareKey("byId", "/namespaces/namespace", "id");
		return m.toDocument(false);
	}
	
	private static boolean isAgentGoodForRouting(String aAgentId, IHDSReader aDeploymentState)
	{
		boolean result=false;
		IHDSReader agent=aDeploymentState.getFirstNodeOrNull("key('byId','"+aAgentId+"')");
		if (agent!=null)
		{
			boolean isCorrect=(Boolean)agent.getFirstValue("deployment/correct");
			result=isCorrect;
		}
		return result;
		
	}
}
