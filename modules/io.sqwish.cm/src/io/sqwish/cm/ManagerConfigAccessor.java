package io.sqwish.cm;

import java.net.URI;

import org.netkernel.container.config.IConfiguration;
import org.netkernel.layer0.boot.BootUtils;
import org.netkernel.layer0.nkf.INKFLocale;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.util.Utils;

public class ManagerConfigAccessor extends StandardAccessorImpl
{
	public ManagerConfigAccessor()
	{
		this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws NKFException
	{
		IConfiguration config = aContext.getKernelContext().getKernel().getConfiguration();
		URI base = URI.create(BootUtils.getInstallPath(config));
		URI configURI=base.resolve(URI.create("etc/sqwishConfig.xml")); 
		String configURIString=configURI.toString();
		
		ManagerConfigRepresentation rep;
		if (aContext.exists(configURIString))
		{
			try
			{	
				IHDSDocument configDoc=aContext.source(configURI.toString(),IHDSDocument.class);
				rep=new ManagerConfigRepresentation(configDoc,aContext);
			}
			catch (Exception e)
			{	aContext.logRaw(INKFLocale.LEVEL_WARNING, "Failed to load etc/sqwishConfig.xml\n"+Utils.throwableToString(e));
				rep=new ManagerConfigRepresentation();
			}
		}
		else
		{	aContext.logRaw(INKFLocale.LEVEL_WARNING, "etc/sqwishConfig.xml does not exist");
			rep=new ManagerConfigRepresentation();
		}
		
		aContext.createResponseFrom(rep);
	}
}
