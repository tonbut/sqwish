package io.sqwish.cm;

import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class StateAggregationAccessor extends StandardAccessorImpl
{
	public StateAggregationAccessor()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		// active:routeClientRequestStatus
		IHDSReader routeClientRequestStatus=aContext.source("active:routeClientRequestStatus",IHDSDocument.class).getReader();
		IHDSReader agentList=aContext.source("active:agentList",IHDSDocument.class).getReader();
		IHDSReader routingTable=aContext.source("active:routingTable",IHDSDocument.class).getReader();
		IHDSReader agentTelemetry=aContext.source("active:agentState",IHDSDocument.class).getReader();
		IHDSReader agentDeployments=aContext.source("active:agentDeploymentState",IHDSDocument.class).getReader();
		IHDSReader cmTelemetry=aContext.source("active:cmInfo",IHDSDocument.class).getReader();
		
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("state");
		
		m.pushNode("clients");
		for (IHDSReader client : routeClientRequestStatus.getNodes("/clientRequestRouter/clients/client"))
		{
			String id=(String)client.getFirstValue("id");
			m.pushNode("client")
			.appendChildren(client);
			m.popNode();
		}
		m.popNode();
		
		
		m.pushNode("namespaces");
		for (IHDSReader namespace : routingTable.getNodes("/namespaces/namespace"))
		{
			String name=(String)namespace.getFirstValue("id");
			Number agentCount=(Number)namespace.getFirstValue("count(agent)");
			
			m.pushNode("namespace")
			.addNode("availableAgents", agentCount.intValue());
			
			IHDSReader namespaceLive=routeClientRequestStatus.getFirstNodeOrNull("key('namespaceById','"+name+"')");
			if (namespaceLive!=null)
			{	
				
				for (IHDSReader nsChild : namespaceLive.getNodes("*"))
				{
					String childName=(String)nsChild.getFirstValue("name()");
					if (!childName.equals("endpoints"))
					{
						m.append(nsChild);
						m.popNode();
					}
				}
				
				//m.appendChildren(namespaceLive);
			}
			else
			{	m.addNode("id", name);
				m.addNode("queue", 0L);
				m.addNode("queueWait", 0L);
				m.addNode("accepted", 0L);
				m.addNode("executed", 0L);
				m.addNode("completed", 0L);
			}
			
			IHDSReader metadata=null;
			try
			{	INKFRequest req=aContext.createRequest("active:namespaceMetadata");
				req.addArgument("namespace", name);
				req.setRepresentationClass(IHDSDocument.class);
				metadata=((IHDSDocument)aContext.issueRequest(req)).getReader();
			}
			catch (Exception e)
			{	// no metadata yet
			}
			
			if (metadata!=null)
			{
				m.pushNode("endpoints");
				for (IHDSReader endpoint : metadata.getNodes("/endpoints/endpoint"))
				{
					String endpointId=(String)endpoint.getFirstValue("id");
					m.pushNode("endpoint")
					.addNode("id", endpointId);
					
					IHDSReader liveEndpointState=namespaceLive!=null?namespaceLive.getFirstNodeOrNull("endpoints/endpoint[id='"+endpointId+"']"):null;
					if (liveEndpointState!=null)
					{
						m.addNode("responseTime", liveEndpointState.getFirstValue("responseTime"));
						m.addNode("executed", liveEndpointState.getFirstValue("executed"));
					}
					else
					{	m.addNode("executed",0L);
					}
					
					m.popNode();
				}
				m.popNode();
			}
			
			
			m.popNode();
		}
		/*
		for (IHDSReader namespace : routeClientRequestStatus.getNodes("/clientRequestRouter/namespaces/namespace"))
		{
			String name=(String)namespace.getFirstValue("id");
			Number agentCount=(Number)routingTable.getFirstValue("count(key('byId','"+name+"')/agent)");
			m.pushNode("namespace")
			.addNode("availableAgents", agentCount.intValue())
			.appendChildren(namespace);
			m.popNode();
		}
		*/
		m.popNode();
		
		
		m.pushNode("agents");
		int agentCount=0;
		int agentGood=0;
		float totalCorePerformance=0.0f;
		float totalUtilisation=0.0f;

		for (IHDSReader agent : agentList.getNodes("/agents/agent"))
		{
			String agentId=(String)agent.getFirstValue("id");
			String agentIP=(String)agent.getFirstValue("ip");
			boolean connected=(Boolean)agent.getFirstValue("connected");
			agentCount++;
			m.pushNode("agent")
			.addNode("id", agentId)
			.addNode("ip", agentIP)
			.addNode("connected", connected);
			
			
			boolean isCorrect=false;
			IHDSReader agentDeployment=agentDeployments.getFirstNodeOrNull("key('byId','"+agentId+"')");
			if (agentDeployment!=null)
			{	isCorrect=(Boolean)agentDeployment.getFirstValue("deployment/correct");
				m.addNode("correct", isCorrect);
				if (isCorrect) agentGood++;
			}
			
			IHDSReader routerStatus=routeClientRequestStatus.getFirstNodeOrNull("key('agentById','"+agentId+"')");
			if (routerStatus!=null)
			{	m.appendChildren(routerStatus);
			}
			else
			{	m.addNode("concurrency", 0L);
				m.addNode("executed", 0L);
				m.addNode("executedRate", 0.0f);
			}
			
			IHDSReader telemetry=agentTelemetry.getFirstNodeOrNull("key('byId','"+agentId+"')/info");
			if (telemetry!=null)
			{	m.appendChildren(telemetry);
			
				float userCPU=((Number)telemetry.getFirstValue("cpu/userCPU")).floatValue();
				float corePerformance=Float.parseFloat(((String)telemetry.getFirstValue("cpu/corePerformance")));
				float cores=Float.parseFloat(((String)telemetry.getFirstValue("cpu/processors")));
				if (isCorrect)
				{
					totalCorePerformance+=corePerformance*cores;
					totalUtilisation+=corePerformance*cores*userCPU;
				}
			}
			
			m.popNode();
		}
		
		m.addNode("count", agentCount);
		m.addNode("countGood", agentGood);
		m.addNode("countBad", agentCount-agentGood);
		float utilisation=totalCorePerformance>0.0f?totalUtilisation/totalCorePerformance:0.0f;
		if (utilisation>100.0f) utilisation=100.0f;
		m.addNode("utilisation",utilisation);
		m.addNode("totalComputePower",totalCorePerformance);
		m.popNode();
		
		//add CM instance info
		m.pushNode("cm");
		m.appendChildren(cmTelemetry.getFirstNode("/info"));
		m.popNode();
				
		
		IHDSDocument result=m.toDocument(false);
		verifyRoutingTable(result,routingTable,aContext);
		
		INKFResponse resp=aContext.createResponseFrom(result);
		//hack cache for nearly 5 seconds just in case multiple clients want it
		resp.setExpiry(INKFResponse.EXPIRY_CONSTANT, System.currentTimeMillis()+4500L);
	}
	
	/** verify routing table has correct number of cores for each agent and if not invalidate
	 * it gets like this as we don't have up to date info when it is first updated after new agent connects
	 */
	private void verifyRoutingTable(IHDSDocument aAggregation, IHDSReader aRoutingTable, INKFRequestContext aContext)
	{
		boolean differences=false;
		for (IHDSReader agent : aAggregation.getReader().getNodes("/state/agents/agent"))
		{
			String id=(String)agent.getFirstValue("id");
			String cores=(String)agent.getFirstValueOrNull("cpu/processors");
			String routingCores=(String)aRoutingTable.getFirstValueOrNull("/namespaces/namespace/agent[id='"+id+"']/cores");
			//System.out.println(id+" "+cores+" "+routingCores);
			if (cores!=null && !cores.equals(routingCores))
			{	differences=true;
				break;
			}
		}
		
		if (differences)
		{
			try
			{	aContext.source("active:routingTableInvalidate");
			}
			catch (Exception e)
			{ ; }
		}
	}
}
