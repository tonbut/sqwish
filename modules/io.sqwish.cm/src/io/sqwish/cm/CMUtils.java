package io.sqwish.cm;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.netkernel.layer0.nkf.INKFAsyncRequestListener;
import org.netkernel.layer0.nkf.INKFLocale;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.util.Utils;

public class CMUtils
{
	public static void notifyAgentError(String aAgentId, String aReason, INKFRequestContext aContext) throws Exception
	{

		INKFRequest req=aContext.createRequest("active:agentError");
		req.addArgumentByValue("instanceId", aAgentId);
		req.addArgumentByValue("error", aReason);
		aContext.issueAsyncRequest(req).setListener(new INKFAsyncRequestListener()
		{
			public void receiveResponse(INKFResponseReadOnly aResponse, INKFRequestContext aContext) throws Exception
			{	aContext.setNoResponse();
			}
			
			public void receiveException(NKFException aException, INKFRequest aRequest, INKFRequestContext aContext) throws Exception
			{	aContext.logRaw(INKFLocale.LEVEL_WARNING, Utils.throwableToString(aException));
				aContext.setNoResponse();
			}
		});
	}
	
	public static String textReplace(String aTemplate, Map<String,String> aTokens)
	{
		String patternString = "%(\\w+?)%";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(aTemplate);

		StringBuffer sb = new StringBuffer();
		while(matcher.find()) {
		    String keyword=matcher.group(1);
		    String replacement2=aTokens.get(keyword);
			matcher.appendReplacement(sb, replacement2);
		}
		matcher.appendTail(sb);	
		return sb.toString();
	}
}
