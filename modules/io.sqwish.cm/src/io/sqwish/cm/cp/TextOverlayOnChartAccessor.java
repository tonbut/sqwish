package io.sqwish.cm.cp;

import java.util.HashMap;
import java.util.Map;

import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

import io.sqwish.cm.CMUtils;

public class TextOverlayOnChartAccessor extends StandardAccessorImpl
{
	public TextOverlayOnChartAccessor()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String operand=aContext.source("arg:operand",String.class);
		String text=aContext.source("arg:text",String.class);
		String colour=aContext.source("arg:colour",String.class);
		String template=aContext.source("res:/io/sqwish/cm/cp/textOverlayTemplate.xml",String.class);
		
		
		Map<String,String> tokens = new HashMap<String,String>();
		tokens.put("CHART",operand);
		tokens.put("TEXT",text);
		tokens.put("COLOUR",colour);
		
		String result=CMUtils.textReplace(template, tokens);
		INKFResponse respOut=aContext.createResponseFrom(result);
		respOut.setMimeType("text/html");
	}
}
