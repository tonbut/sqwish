package io.sqwish.cm.cp;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.layer0.representation.IHDSNode;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class ChartRestAPI extends StandardAccessorImpl
{
	public ChartRestAPI()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String type=aContext.getThisRequest().getArgumentValue("type");
		String entity=aContext.getThisRequest().getArgumentValue("entity");
		
		String field=aContext.getThisRequest().getArgumentValue("field");
		field=field.replaceAll("-", "/");
		IHDSNode params=aContext.source("httpRequest:/params",IHDSNode.class);
		
		//colour
		String colour=(String)params.getFirstValue("colour");
		if (colour==null) colour="#8080FF";
		else colour="#"+colour;
		
		int[] colours=hex2Rgb(colour);
		String fill=String.format("rgba(%d,%d,%d,0.2)", colours[0],colours[1],colours[2]);
		String stroke=String.format("rgba(%d,%d,%d,0.6)", colours[0],colours[1],colours[2]);
		String textColour=String.format("rgba(%d,%d,%d,1.0)", colours[0],colours[1],colours[2]);
		
		//interpolate
		String interpolate=(String)params.getFirstValue("interpolate");
		if (interpolate==null) interpolate="basis";
		
		//operator
		String op=(String)params.getFirstValue("op");
		if (op==null) op="";
		
		String valueMultiply="1";
		String queryOp="average";
		switch(op) {
		case "rateSec":
			valueMultiply="0.1";
			queryOp="diff";
			break;
		case "sample":
			queryOp="sample";
			break;
		}
		
		List<String> entities=null;
		if (entity.equals("+"))
		{	
			
			//all entities summed
			entities=new ArrayList<String>();
			if (type.equals("agent"))
			{	
				IHDSReader agentList=aContext.source("active:agentList",IHDSDocument.class).getReader();
				for (Object agent : agentList.getValues("/agents/agent/id"))
				{	entities.add((String)agent);
				}
			}
			else if (type.equals("namespace"))
			{	
				IHDSReader agentList=aContext.source("active:agentAssignments",IHDSDocument.class).getReader();
				for (Object namespace : agentList.getValues("/assignments/namespaces/namespace/@id"))
				{	entities.add((String)namespace);
				}
			}
			else if (type.equals("client"))
			{	
				IHDSReader clientList=aContext.source("active:routeClientRequestStatus",IHDSDocument.class).getReader();
				for (Object client : clientList.getValues("/clientRequestRouter/clients/client/id"))
				{	entities.add((String)client);
				}
			}
		}
		else if (entity.equals("-"))
		{	//access collect properties
			entity="";
		}
		
		//System.out.println("ChartRestAPI "+type+" "+entity+" "+field+" "+fill+" "+op);
		
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("query")
		.addNode("start", Long.toString(-5*60*1000L))
		.addNode("period", "5000");
		
		if (entities==null)
		{
			m.pushNode("value")
			.addNode(type, entity)
			.addNode("field", field);
			m.addNode("op", queryOp);
			m.popNode();
		}
		else
		{
			for (String entity2 : entities)
			{
				m.pushNode("value")
				.addNode(type, entity2)
				.addNode("field", field);
				m.addNode("op", queryOp);
				m.popNode();
			}
			
		}
		
		INKFRequest req=aContext.createRequest("active:historicalQuery");
		req.addArgumentByValue("operator", m.toDocument(false));
		req.setRepresentationClass(IHDSDocument.class);
		IHDSDocument resultSet=(IHDSDocument)aContext.issueRequest(req);
		
		if ((entity.equals("+")))
		{
			resultSet=generateSumResultSet(resultSet);
		}
		
		
		m=HDSFactory.newDocument();
		m.pushNode("chart")
		.addNode("type", "TimeSeriesData")
		.addNode("excludeJS", "true")
		.addNode("legend", "false")
		.addNode("backgroundColor", "rgba(255,255,255,0)")
		.addNode("axisColor", "rgba(255,255,255,0)")
		.addNode("textColor", "#444")
		.addNode("width", "100")
		.addNode("height", "50")
		.addNode("yAxisBottom", "0")
		.addNode("xAxis", "t")
		.addNode("xAxisTicks", "none")
		.addNode("yAxisTicks", "range")
		.pushNode("dataSets")
		.pushNode("dataSet")
		.addNode("id", "v1")
		.addNode("type", "area")
		.addNode("interpolate", interpolate)
		.addNode("valueMultiply", valueMultiply)
		.addNode("fill", fill)
		.addNode("baseline", "0")
		.addNode("stroke",stroke );
		
		req=aContext.createRequest("active:declarativeTimeseriesChart");
		req.addArgumentByValue("operator", m.toDocument(false));
		req.addArgumentByValue("operand", resultSet);
		INKFResponseReadOnly respIn=aContext.issueRequestForResponse(req);
		
		
		Object value=resultSet.getReader().getFirstValueOrNull("/data/row[last()]/v1");
		String text="";
		if (value!=null && value instanceof Number)
		{
			double d=((Number)value).doubleValue()*Double.parseDouble(valueMultiply);
			int l=(int)Math.log10(Math.abs(d));
			int digits=2-l; 
			if (digits<0) digits=0;
			if (digits>2) digits=2;
			NumberFormat nf = NumberFormat.getInstance();
			nf.setMaximumFractionDigits(digits);
			text=nf.format(d);
		}
		
		req=aContext.createRequest("active:overlayTextOnChart");
		req.addArgumentFromResponse("operand", respIn);
		req.addArgumentByValue("text", text);
		req.addArgumentByValue("colour", textColour);
		respIn=aContext.issueRequestForResponse(req);
		
		aContext.createResponseFrom(respIn);
	}
	
	public static int[] hex2Rgb(String colorStr) {
	    return new int[] {
	            Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
	            Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
	            Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) };
	}
	
	private IHDSDocument generateSumResultSet(IHDSDocument aResultSet)
	{
		//System.out.println(aResultSet);
		
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("data");
		for (IHDSReader row : aResultSet.getReader().getNodes("/data/row"))
		{
			m.pushNode("row");
			double sum=0;
			boolean exists=false;
			for (IHDSReader child : row.getNodes("*"))
			{
				String name=(String)child.getFirstValue("name()");
				Object value=child.getFirstValue(".");
				if (name.startsWith("v"))
				{
					if (value instanceof Number)
					{
						sum+=((Number)value).doubleValue();
						exists=true;
					}
				}
				else
				{	m.addNode(name, value);
				}
			}
			m.addNode("v1", exists?sum:null);
			m.popNode();
		}
		
		return m.toDocument(false);
	}
}
