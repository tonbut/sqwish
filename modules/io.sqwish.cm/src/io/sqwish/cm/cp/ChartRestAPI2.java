package io.sqwish.cm.cp;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.layer0.representation.IHDSNode;
import org.netkernel.layer0.representation.IReadableBinaryStreamRepresentation;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class ChartRestAPI2 extends StandardAccessorImpl
{
	public ChartRestAPI2()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String json=aContext.source("httpRequest:/body",String.class);
		//System.out.println("chart2 "+json);
		INKFRequest req=aContext.createRequest("active:JSONToHDS");
		req.addArgumentByValue("operand", json);
		req.setRepresentationClass(IHDSDocument.class);
		IHDSReader body=((IHDSDocument)aContext.issueRequest(req)).getReader();
		//System.out.println("chart2 "+body);
		
		IHDSMutator q=HDSFactory.newDocument();
		q.pushNode("query")
		.addNode("start", Long.toString(-5*60*1000L))
		.addNode("period", "5000");
		
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("chart")
		.addNode("type", "TimeSeriesData")
		.addNode("excludeJS", "true")
		.addNode("legend", "false")
		.addNode("backgroundColor", "rgba(255,255,255,0)")
		.addNode("axisColor", "rgba(255,255,255,0)")
		.addNode("textColor", "#444")
		.addNode("width", "100")
		.addNode("height", "50")
		.addNode("yAxisBottom", "0")
		.addNode("xAxis", "t")
		.addNode("xAxisTicks", "none")
		.addNode("yAxisTicks", "range")
		.pushNode("dataSets");
		
		int n=1;
		String textDataNode=null;
		String textValueMultiply=null;
		String textTextColour=null;
		List<IHDSReader> nodes=body.getNodes("/data__A/data");
		for (IHDSReader data : nodes)
		{
			String type=(String)data.getFirstValue("type");
			String entity=(String)data.getFirstValue("entity");
			String field=(String)data.getFirstValue("field");
			field=field.replaceAll("-", "/");
			String colour=(String)data.getFirstValueOrNull("colour");
			String interpolate=(String)data.getFirstValueOrNull("interpolate");
			String op=(String)data.getFirstValueOrNull("op");
			boolean text=data.getFirstValueOrNull("text")!=null;
			
			if (colour==null) colour="#8080FF";
			int[] colours=hex2Rgb(colour);
			
			String fill=String.format("rgba(%d,%d,%d,1.0)", mix(colours[0],255,0.2f),mix(colours[1],255,0.2f),mix(colours[2],255,0.2f));
			String stroke=String.format("rgba(%d,%d,%d,1.0)", mix(colours[0],255,0.6f),mix(colours[1],255,0.6f),mix(colours[2],255,0.6f));
			String textColour=String.format("rgba(%d,%d,%d,1.0)", colours[0],colours[1],colours[2]);
			
			//String fill=String.format("rgba(%d,%d,%d,0.2)", colours[0],colours[1],colours[2]);
			//String stroke=String.format("rgba(%d,%d,%d,0.6)", colours[0],colours[1],colours[2]);
			//String textColour=String.format("rgba(%d,%d,%d,1.0)", colours[0],colours[1],colours[2]);
			
			if (interpolate==null) interpolate="basis";
			
			if (op==null) op="";
			
			String valueMultiply="1";
			String queryOp="average";
			switch(op) {
			case "rateSec":
				valueMultiply="0.1";
				queryOp="diff";
				break;
			case "sample":
				queryOp="sample";
				break;
			}
			
			List<String> entities=null;
			if (entity.equals("+"))
			{	
				
				//all entities summed
				entities=new ArrayList<String>();
				if (type.equals("agent"))
				{	
					IHDSReader agentList=aContext.source("active:agentList",IHDSDocument.class).getReader();
					for (Object agent : agentList.getValues("/agents/agent/id"))
					{	entities.add((String)agent);
					}
				}
				else if (type.equals("namespace"))
				{	
					IHDSReader agentList=aContext.source("active:agentAssignments",IHDSDocument.class).getReader();
					for (Object namespace : agentList.getValues("/assignments/namespaces/namespace/@id"))
					{	entities.add((String)namespace);
					}
				}
			}
			else if (entity.equals("-"))
			{	//access collect properties
				entity="";
			}
			
			if (entities==null)
			{
				q.pushNode("value")
				.addNode(type, entity)
				.addNode("field", field);
				q.addNode("op", queryOp);
				q.popNode();
			}
			else
			{
				for (String entity2 : entities)
				{
					q.pushNode("value")
					.addNode(type, entity2)
					.addNode("field", field);
					q.addNode("op", queryOp);
					q.popNode();
				}
				
			}
			
			
			String dataNode="v"+Integer.toString(n++);
			if (text || nodes.size()==1)
			{	textDataNode=dataNode;
				textValueMultiply=valueMultiply;
				textTextColour=textColour;
			}
			
			m.pushNode("dataSet")
			.addNode("id", dataNode)
			.addNode("type", "area")
			.addNode("interpolate", interpolate)
			.addNode("valueMultiply", valueMultiply)
			.addNode("fill", fill)
			.addNode("baseline", "0")
			.addNode("stroke",stroke )
			.popNode();
			
		}
		
		//System.out.println(q);
		req=aContext.createRequest("active:historicalQuery");
		req.addArgumentByValue("operator", q.toDocument(false));
		req.setRepresentationClass(IHDSDocument.class);
		IHDSDocument resultSet=(IHDSDocument)aContext.issueRequest(req);
		//System.out.println(resultSet);
		
		//TODO
		/*
		if ((entity.equals("+")))
		{
			resultSet=generateSumResultSet(resultSet);
		}
		 */
		
		//System.out.println(m);
		
		req=aContext.createRequest("active:declarativeTimeseriesChart");
		req.addArgumentByValue("operator", m.toDocument(false));
		req.addArgumentByValue("operand", resultSet);
		INKFResponseReadOnly respIn=aContext.issueRequestForResponse(req);
		
		
		Object value=resultSet.getReader().getFirstValueOrNull("/data/row[last()]/"+textDataNode);
		String text="";
		if (value!=null && value instanceof Number)
		{
			double d=((Number)value).doubleValue()*Double.parseDouble(textValueMultiply);
			int l=(int)Math.log10(Math.abs(d));
			int digits=2-l; 
			if (digits<0) digits=0;
			if (digits>2) digits=2;
			NumberFormat nf = NumberFormat.getInstance();
			nf.setMaximumFractionDigits(digits);
			text=nf.format(d);
		}
		
		req=aContext.createRequest("active:overlayTextOnChart");
		req.addArgumentFromResponse("operand", respIn);
		req.addArgumentByValue("text", text);
		req.addArgumentByValue("colour", textTextColour);
		respIn=aContext.issueRequestForResponse(req);
		
		
		aContext.createResponseFrom(respIn);
	}
	
	public static int[] hex2Rgb(String colorStr) {
	    return new int[] {
	            Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
	            Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
	            Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) };
	}
	
	public static int mix(int aC1, int aC2, float aMix)
	{
		float v=((float)aC1)*aMix+((float)aC2)*(1-aMix);
		return (int)v;
	}
}
