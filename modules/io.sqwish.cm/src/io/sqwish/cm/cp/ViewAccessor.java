package io.sqwish.cm.cp;

import java.util.HashMap;
import java.util.Map;

import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

import io.sqwish.cm.CMUtils;

public class ViewAccessor extends StandardAccessorImpl
{
	public ViewAccessor()
	{
		this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String path=aContext.getThisRequest().getArgumentValue("path");
		if (path.equals(""))
		{
			Object page=aContext.source("res:/io/sqwish/cm/cp/pageTemplate.xml");
			INKFResponse resp=aContext.createResponseFrom(page);
			resp.setHeader("WrappedControlPanel", true);
		}
		else if (path.startsWith("/agent/"))
		{
			String agentId=path.substring(7);
			Map<String,String> tokens = new HashMap<String,String>();
			tokens.put("AGENT_ID",agentId);
			
			String page=aContext.source("res:/io/sqwish/cm/cp/agentPageTemplate.xml",String.class);
			page=CMUtils.textReplace(page, tokens);
			INKFResponse resp=aContext.createResponseFrom(page);
			String title="Sqwish Agent "+agentId;
			resp.setHeader("WrappedControlPanel", title);
		}
		else if (path.startsWith("/ingress/"))
		{	boolean enabled=Boolean.parseBoolean(path.substring(9));
			aContext.source("active:cmState-Ingress-"+(enabled?"Enable":"Disable") );
			aContext.createResponseFrom("done").setExpiry(INKFResponse.EXPIRY_ALWAYS);
		}
		else if (path.startsWith("/egress/"))
		{	boolean enabled=Boolean.parseBoolean(path.substring(8));
			aContext.source("active:cmState-Egress-"+(enabled?"Enable":"Disable") );
			aContext.createResponseFrom("done").setExpiry(INKFResponse.EXPIRY_ALWAYS);
		}
		else if (path.startsWith("/flush"))
		{
			aContext.source("active:cmState-Flush");
			aContext.createResponseFrom("done").setExpiry(INKFResponse.EXPIRY_ALWAYS);
		}
	}
}
