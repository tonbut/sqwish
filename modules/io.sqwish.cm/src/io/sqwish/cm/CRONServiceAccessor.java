package io.sqwish.cm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.netkernel.layer0.nkf.INKFLocale;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class CRONServiceAccessor extends StandardAccessorImpl
{
	private Map<String,String> mReplicaSetHashes=new HashMap<>();
	private boolean mFirstInvocationFlag=true;
	
	public CRONServiceAccessor()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		pollForChangedReplicaSets(aContext);
		pollAgentInfo(aContext);
	}
	
	private void pollAgentInfo(INKFRequestContext aContext) throws Exception
	{
		//IHDSDocument aggregateState=aContext.source("active:stateAggregation",IHDSDocument.class);
		INKFRequest req=aContext.createRequest("active:persistState");
		req.addArgument("operand", "active:stateAggregation");
		aContext.issueRequest(req);
	}
	
	
	private void pollForChangedReplicaSets(INKFRequestContext aContext) throws Exception
	{
		ManagerConfigRepresentation config=aContext.source("active:sqwishConfig",ManagerConfigRepresentation.class);
		Set<String> changes=new HashSet<>();
		
		//look for new and changed sets
		for (String replicaSetId : config.getReplicaSets())
		{
			String currentHash=(String)config.getReplicaSetDefinitionForId(replicaSetId).getReader().getFirstValue("/*/hash");
			String previousHash=mReplicaSetHashes.get(replicaSetId);
			if (previousHash==null || !previousHash.equals(currentHash))
			{
				changes.add(replicaSetId);
				mReplicaSetHashes.put(replicaSetId,currentHash);
			}
		}
		//look for removed sets
		for (String replicaSetId : mReplicaSetHashes.keySet())
		{	if (!config.getReplicaSets().contains(replicaSetId))
			{	changes.add(replicaSetId);
				mReplicaSetHashes.remove(replicaSetId);
			}
		}
		
		for (String replicaSetId : changes)
		{
			if (!mFirstInvocationFlag)
			{
				String msg="Changes detected in replica set "+replicaSetId;
				aContext.logRaw(INKFLocale.LEVEL_INFO, msg);
				
				INKFRequest req=aContext.createRequest("active:invalidateNamespaceMetadata");
				req.addArgument("namespace", replicaSetId);
				aContext.issueRequest(req);
			}
		}
		
		mFirstInvocationFlag=false;
		
		aContext.source("active:agentDeploymentNotification");
	}
}
