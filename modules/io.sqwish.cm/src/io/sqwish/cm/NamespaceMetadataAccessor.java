package io.sqwish.cm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

import org.netkernel.layer0.nkf.INKFExpiryFunction;
import org.netkernel.layer0.nkf.INKFLocale;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.layer0.representation.impl.HDSFactory;
import org.netkernel.layer0.urii.ParsedIdentifierImpl;
import org.netkernel.layer0.util.GoldenThreadExpiryFunction;
import org.netkernel.layer0.util.TimedExpiryFunction;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.util.Utils;

public class NamespaceMetadataAccessor extends StandardAccessorImpl
{
	private final Map<String,Metadata> mNamespaceToMetadata=new HashMap();
	
	public NamespaceMetadataAccessor()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String namespace=aContext.getThisRequest().getArgumentValue("namespace");
		String action=aContext.getThisRequest().getArgumentValue(ParsedIdentifierImpl.ARG_ACTIVE_TYPE);
		if (action.equals("invalidateNamespaceMetadata"))
		{	onInvalidate(namespace,aContext);
		}
		else
		{	onSourceInner(namespace,aContext);
		}
	}
	
	public void onInvalidate(String aNamespace, INKFRequestContext aContext) throws Exception
	{
		//aContext.logRaw(INKFLocale.LEVEL_INFO, "onInvalidate "+aNamespace);
		Metadata old=mNamespaceToMetadata.get(aNamespace);
		if (old!=null)
		{
			old.invalidate();
		}
	}
	
	private Semaphore mSema=new Semaphore(1);
	
	public void onSourceInner(final String aNamespace, INKFRequestContext aContext) throws Exception
	{
		try
		{
			ClientRequestRouter.getInstance().ensureClientIsRegistered(aContext, aNamespace);
			
			ManagerConfigRepresentation rep=aContext.source("active:sqwishConfig",ManagerConfigRepresentation.class);
			if (rep.getReplicaSetDefinitionForId(aNamespace)==null)
			{	throw new NKFException("Unknown namespace",aNamespace);
			}
			
			IHDSReader replicaSetDefn=rep.getReplicaSetDefinitionForId(aNamespace).getReader();
			
			String replicaSetHash=(String)replicaSetDefn.getFirstValue("/*/hash");
			//aContext.logRaw(INKFLocale.LEVEL_INFO, "NamespaceMetadataAccessor "+aNamespace+" "+replicaSetHash);
			
			Metadata metadata=mNamespaceToMetadata.get(aNamespace);
			if (metadata==null || isMetadataExpired(metadata,replicaSetHash))
			{
				IHDSReader routingTable=aContext.source("active:routingTable",IHDSDocument.class).getReader();
			
			
				IHDSReader firstAgent=routingTable.getFirstNodeOrNull("/namespaces/namespace[id='"+aNamespace+"']/agent");
				if (firstAgent==null)
				{	throw new NKFException("Metadata for namespace not available yet",aNamespace);
				}
				else
				{	//TODO one semaphore per namespace
					boolean acquired=mSema.tryAcquire();
					if (acquired)
					{
						IHDSDocument doc;
						try
						{
							//request metadata from agent
							INKFRequestContext agentContext=(INKFRequestContext)firstAgent.getFirstValue("context");
							INKFRequest req=agentContext.createRequest("active:deployedModulesMetadata");
							req.addArgumentByValue("namespace", aNamespace);
							req.setRepresentationClass(IHDSDocument.class);
							doc=(IHDSDocument)agentContext.issueAsyncRequest(req).join(rep.getAgentTimeout());
						}
						finally
						{	mSema.release();
						}
						if (doc!=null)
						{	
							metadata=new Metadata(doc,replicaSetHash);
							mNamespaceToMetadata.put(aNamespace, metadata);
							//System.out.println(doc);
						}
						else
						{	
							metadata=new Metadata(); //an empty set of endpoints that will expire quickly
							mNamespaceToMetadata.put(aNamespace, metadata);
							
							//use existing metadata if any and mark agent as bad
							String agentId=(String)firstAgent.getFirstValue("id");
							CMUtils.notifyAgentError(agentId, "timeout on metadata request", aContext);
						}
					}
					else
					{	throw new NKFException("Metadata for namespace reenterrant call",aNamespace);
					}
				}
			}
			
			INKFResponse resp=aContext.createResponseFrom(metadata.getMetadataDocument());
			resp.setExpiry(INKFResponse.EXPIRY_FUNCTION,metadata.getExpiry());
		}
		catch (NKFException e)
		{
			aContext.logRaw(INKFLocale.LEVEL_WARNING, "Unhandled exception in NamespaceMetadataAccessor: "+e.getDeepestId()+": "+e.getDeepestMessage());
			//add validity duration to expiry to minimise client re-requests on error
			INKFResponse resp=aContext.createResponseFrom(null);
			resp.setExpiry(INKFResponse.EXPIRY_CONSTANT,System.currentTimeMillis()+1000L);
		}
	}
	
	private boolean isMetadataExpired(Metadata aMetadata, String aCurrentReplicaSetHash)
	{	
		return !aMetadata.getReplicaSetHash().equals(aCurrentReplicaSetHash);
	}

	private static class Metadata
	{
		private final IHDSDocument mMetadataDocument;
		private final String mReplicaSetHash;
		private final INKFExpiryFunction mExpiry;
		
		public Metadata()
		{
			IHDSMutator m=org.netkernel.mod.hds.HDSFactory.newDocument();
			m.pushNode("endpoints");
			mMetadataDocument=m.toDocument(false);
			mReplicaSetHash="";
			mExpiry=new TimedExpiryFunction("Namespace Metadata", System.currentTimeMillis()+500L);
		}
		
		public Metadata(IHDSDocument aMetadataDocument, String aReplicaSetHash)
		{
			mMetadataDocument=aMetadataDocument;
			mReplicaSetHash=aReplicaSetHash;
			mExpiry=new GoldenThreadExpiryFunction("Namespace Metadata");
		}
		public IHDSDocument getMetadataDocument() { return mMetadataDocument; }
		public String getReplicaSetHash() { return mReplicaSetHash; }
		public INKFExpiryFunction getExpiry() { return mExpiry; }
		public void invalidate()
		{	if (mExpiry instanceof GoldenThreadExpiryFunction)
			{	((GoldenThreadExpiryFunction)mExpiry).invalidate();
			}
		}
	}
}
