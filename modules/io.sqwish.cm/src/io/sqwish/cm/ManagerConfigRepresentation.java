package io.sqwish.cm;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.netkernel.layer0.boot.BootUtils;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.util.XMLReadable;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.StandardModule;

public class ManagerConfigRepresentation
{
	private final Map<String,String> mHashToSource=new HashMap<>();
	private final Map<String,IHDSDocument> mIdToReplicaSet=new HashMap<>();
	private Set<String> mReplicaSets=Collections.EMPTY_SET;
	private Map<String,String> mUsers=new HashMap<>();
	private boolean mTLS;
	private String mKeystore;
	private String mKeystorePassword;
	private String mKeyPassword;
	
	/** default null config **/
	public ManagerConfigRepresentation()
	{
	}
	
	public ManagerConfigRepresentation(IHDSDocument aConfig, INKFRequestContext aContext) throws Exception
	{
		
		IHDSReader r=aConfig.getReader();
		for (IHDSReader replicaSet : r.getNodes("/sqwish/replicaSet"))
		{
			String setId=(String)replicaSet.getFirstValue("@id");
			IHDSMutator m=HDSFactory.newDocument();
			m.pushNode("replicaSet")
			.addNode("@id", setId);
			
			int replicaSetHash=0;
			for (IHDSReader module : replicaSet.getNodes("module"))
			{
				String moduleSrc=(String)module.getFirstValue("@src");
				XMLReadable defn=BootUtils.getXMLFrom(URI.create(moduleSrc),StandardModule.MODULE_DEFN);
				String moduleURI=defn.getText("/module/meta/identity/uri");
				String moduleVersion=defn.getText("/module/meta/identity/version");
				int hash=(moduleURI+moduleVersion).hashCode();
				String hashString=String.format("%08X",hash);
				
				//MD5
				INKFRequest req = aContext.createRequest("active:md5");
				req.addArgument("operand", moduleSrc);
				req.setRepresentationClass(String.class);
				String md5=(String)aContext.issueRequest(req);
				replicaSetHash^=md5.hashCode();
				
				//System.out.println(hashString+" "+moduleURI+" "+moduleVersion+" "+md5);
				mHashToSource.put(hashString,moduleSrc);
				m.pushNode("module")
				.addNode("id", hashString)
				.addNode("uri", moduleURI)
				.addNode("version", moduleVersion)
				.addNode("content_md5", md5)
				.popNode();
				
			}
			String hashString=String.format("%08X",replicaSetHash);
			//System.out.println("new ManagerConfigRepresentation "+hashString);
			m.addNode("hash", hashString);
			mIdToReplicaSet.put(setId, m.toDocument(false));
			mReplicaSets=Collections.unmodifiableSet(mIdToReplicaSet.keySet());
		}
		
		//users
		for (IHDSReader user : r.getNodes("/sqwish/users/user"))
		{	String username=(String)user.getFirstValue("name");
			String passwordHash=(String)user.getFirstValue("password");
			mUsers.put(username,passwordHash);
		}
		
		//tls
		IHDSReader tls=r.getFirstNodeOrNull("/sqwish/tls");
		if (tls!=null)
		{	
			mTLS=true;
			mKeystore=(String)tls.getFirstValue("keystore");
			mKeystorePassword=(String)tls.getFirstValue("keyStorePassword");
			mKeyPassword=(String)tls.getFirstValue("keyPassword");
		}
		
	}
	
	public boolean isTLS()
	{	return mTLS;
	}
	
	public String getTLSKeystore()
	{	return mKeystore;
	}
	
	public String getTLSKeystorePassword()
	{	return mKeystorePassword;
	}
	
	public String getTLSKeyPassword()
	{	return mKeyPassword;
	}
	
	public String getPasswordHashFor(String aUsername)
	{	return mUsers.get(aUsername);
	}
	
	public long getAgentTimeout()
	{	return 1000L;
	}
	
	public String getSourceForHash(String aHash)
	{	return mHashToSource.get(aHash);
	}
	
	public IHDSDocument getReplicaSetDefinitionForId(String aId)
	{	return mIdToReplicaSet.get(aId);
	}
	
	public Set<String> getReplicaSets()
	{	return mIdToReplicaSet.keySet();
	}
}
