package io.sqwish.cm;

import javax.swing.text.DefaultEditorKit.CutAction;

import org.netkernel.layer0.nkf.INKFLocale;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.urii.ParsedIdentifierImpl;
import org.netkernel.layer0.util.GoldenThreadExpiryFunction;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.util.Utils;

public class NamespaceToAgentAssignmentsAccessor extends StandardAccessorImpl
{
	
	private IHDSDocument mAssignmentsList;
	private GoldenThreadExpiryFunction mListExpiry;
	
	public NamespaceToAgentAssignmentsAccessor()
	{	this.declareThreadSafe();
	
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("assignments")
		.addNode("namespaces",null)
		.addNode("agents", null);
		m.declareKey("namespaceById", "/assignments/namespaces/namespace", "@id");
		m.declareKey("agentById", "/assignments/agents/agent", "@id");
		mAssignmentsList=m.toDocument(false);
		mListExpiry = new GoldenThreadExpiryFunction("NamespaceToAgentAssignmentsAccessor");
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String action = aContext.getThisRequest().getArgumentValue(ParsedIdentifierImpl.ARG_ACTIVE_TYPE);
		switch (action)
		{	case "agentUpdateAssignments": onUpdateAssignments(aContext); break;
			case "agentAssignments": onAssignmentsList(aContext); break;
		}
	}
	
	private void onAssignmentsList(INKFRequestContext aContext) throws Exception
	{	INKFResponse resp=aContext.createResponseFrom(mAssignmentsList);
		resp.setExpiry(INKFResponse.EXPIRY_MIN_FUNCTION_DEPENDENT,mListExpiry);
	}
	
	private void onUpdateAssignments(INKFRequestContext aContext) throws Exception
	{
		try
		{
			IHDSReader agentList=aContext.source("active:agentList",IHDSDocument.class).getReader();
			//System.out.println("AgentListChangedAccessor "+agentList);

			ManagerConfigRepresentation config=aContext.source("active:sqwishConfig",ManagerConfigRepresentation.class);
			
			//hack - assign all agents to all namespaces

			IHDSMutator m=HDSFactory.newDocument();
			m.pushNode("assignments");
			m.pushNode("namespaces");
			for (String namespace : config.getReplicaSets())
			{	m.pushNode("namespace").addNode("@id", namespace);
				for (Object agentId: agentList.getValues("/agents/agent/id"))
				{	m.addNode("agent", agentId);
				}
				m.popNode();
			}
			m.popNode();
			m.pushNode("agents");
			for (IHDSReader agent: agentList.getNodes("/agents/agent"))
			{
				String agentId=(String)agent.getFirstValue("id");
				m.pushNode("agent").addNode("@id", agentId);
				for (String namespace : config.getReplicaSets())
				{
					m.addNode("namespace", namespace);
				}
				m.popNode();
			}
			m.popNode();
			
			m.declareKey("namespaceById", "/assignments/namespaces/namespace", "@id");
			m.declareKey("agentById", "/assignments/agents/agent", "@id");
			IHDSDocument assignmentsList=m.toDocument(false);
			mAssignmentsList=assignmentsList;
			GoldenThreadExpiryFunction expiry=mListExpiry;
			mListExpiry=new GoldenThreadExpiryFunction("NamespaceToAgentAssignmentsAccessor");
			expiry.invalidate();
			
			//notifyAgents(agentList,assignmentsList.getReader(),config,aContext);
			INKFRequest req=aContext.createRequest("active:agentDeploymentNotification");
			aContext.issueAsyncRequest(req);
			
			
		}
		catch (Exception e)
		{
			aContext.logRaw(INKFLocale.LEVEL_WARNING, Utils.throwableToString(e));
		}
	}
	
	/*
	private void notifyAgents(IHDSReader aAgentList, IHDSReader aAssignmentsList, ManagerConfigRepresentation aConfig, INKFRequestContext aContext) throws Exception
	{
		
		for (IHDSReader agent: aAgentList.getNodes("/agents/agent"))
		{
			String agentId=(String)agent.getFirstValue("id");
			INKFRequestContext agentContext=(INKFRequestContext)agent.getFirstValue("context");
			
			IHDSMutator m=HDSFactory.newDocument();
			m.pushNode("replicaSets");
			
			IHDSReader assignments=aAssignmentsList.getFirstNode("key('agentById','"+agentId+"')");
			for (IHDSReader namespace : assignments.getNodes("namespace"))
			{
				String namespaceId=(String)namespace.getFirstValue(".");
				IHDSDocument replicaSet=aConfig.getReplicaSetDefinitionForId(namespaceId);
			
				m.appendChildren(replicaSet.getReader());
			}
			

			INKFRequest req=agentContext.createRequest("active:sinkDeployment");
			req.addArgumentByValue("operand", m.toDocument(false));
			agentContext.issueRequest(req);
		}
	}
	*/
}
