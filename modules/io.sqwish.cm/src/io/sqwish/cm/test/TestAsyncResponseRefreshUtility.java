package io.sqwish.cm.test;

import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.layer0.urii.ParsedIdentifierImpl;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.util.Utils;

import io.sqwish.cm.AsyncResponseRefreshUtility;

public class TestAsyncResponseRefreshUtility extends StandardAccessorImpl
{
	private long mStart;
	
	
	public TestAsyncResponseRefreshUtility()
	{	this.declareThreadSafe();
		
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String type=aContext.getThisRequest().getArgumentValue(ParsedIdentifierImpl.ARG_ACTIVE_TYPE);
		if (type.equals("TestAsyncResponseRefreshUtility-Inner"))
		{
			System.out.println("*START... "+(System.currentTimeMillis()-mStart));
			Thread.sleep(50);
			long now=System.currentTimeMillis();
			String msg=Long.toString(now-mStart);
			INKFResponse resp=aContext.createResponseFrom(msg);
			resp.setExpiry(INKFResponse.EXPIRY_CONSTANT,now+100L);
			//resp.setExpiry(INKFResponse.EXPIRY_ALWAYS);
			System.out.println("*NEW "+msg);
		}
		/*
		else if (type.equals("TestAsyncResponseRefreshUtility-Layer"))
		{
			System.out.println("here");
			//INKFResponse respIn=mArfu.getResponse(aContext);
			INKFResponse resp=aContext.createResponseFrom(respIn);
		}
		*/
		else
		{
			mStart=System.currentTimeMillis();
			
			INKFRequest req=aContext.createRequest("active:TestAsyncResponseRefreshUtility-Inner");
			req.setRepresentationClass(String.class);
			AsyncResponseRefreshUtility arfu=new AsyncResponseRefreshUtility(req);
			
			
			
			Thread t1=new Runner("A", arfu, aContext);
			Thread t2=new Runner("B", arfu, aContext);
			t1.start();
			Thread.sleep(5);
			t2.start();
			t1.join();
			t2.join();
		}
	}
	
	private static class Runner extends Thread
	{
		private INKFRequestContext mContext;
		private String mName;
		private AsyncResponseRefreshUtility mArfu;
		public Runner(String aName, AsyncResponseRefreshUtility arfu, INKFRequestContext aContext)
		{
			mName=aName;
			mContext=aContext;
			mArfu=arfu;
		}
		public void run()
		{
			try
			{	process(mContext);
			}
			catch (Exception e)
			{	System.out.println(Utils.throwableToString(e));
			}
		}
		
		private void process(INKFRequestContext aContext) throws Exception
		{
			for (int i=0; i<100; i++)
			{
				long t0=System.currentTimeMillis();
				INKFResponse resp=mArfu.getResponse(aContext);
				//INKFResponseReadOnly<String> resp=aContext.sourceForResponse("active:TestAsyncResponseRefreshUtility-Layer", String.class);
				t0=System.currentTimeMillis()-t0;
				System.out.println(mName+" "+i+" "+t0+" "+resp.getRepresentation());
				long delay=5+(int)(Math.random()*10.0);
				Thread.sleep(delay);
			}
			
		}
		
	}
}
