package io.sqwish.cm;

import java.util.HashMap;
import java.util.Map;

import org.netkernel.layer0.nkf.INKFAsyncRequestListener;
import org.netkernel.layer0.nkf.INKFLocale;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.layer0.nkf.impl.NKFContextImpl;
import org.netkernel.layer0.nkf.impl.NKFResponseImpl;
import org.netkernel.layer0.urii.ParsedIdentifierImpl;
import org.netkernel.layer0.util.GoldenThreadExpiryFunction;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.request.IResponse;
import org.netkernel.util.Utils;

public class AgentConnectAccessor extends StandardAccessorImpl
{
	private final Map<String, Client> mClients = new HashMap<>();
	private GoldenThreadExpiryFunction mAgentListExpiry;
	
	public AgentConnectAccessor()
	{	declareThreadSafe();
		cutExpiry();
	}
	
	private void cutExpiry()
	{	GoldenThreadExpiryFunction expiry=mAgentListExpiry;
		mAgentListExpiry=new GoldenThreadExpiryFunction("AgentConnectAccessor");
		if (expiry!=null)
		{	expiry.invalidate();
		}
	}

	@Override
	public void onSource(final INKFRequestContext aContext) throws Exception
	{
		String action = aContext.getThisRequest().getArgumentValue(ParsedIdentifierImpl.ARG_ACTIVE_TYPE);
		
		if (action.equals("agentConnect"))
		{
			final String instanceId=aContext.source("arg:instanceId",String.class);
			final String remoteInfo=(String)aContext.getThisRequest().getHeaderValue("NKP_REMOTE_INFO");
			aContext.setNoResponse();
			INKFRequest req=aContext.createRequest("active:agentHold");
			req.addArgumentByValue("instanceId", instanceId);
			req.addArgumentByValue("remoteInfo", remoteInfo);
			aContext.issueAsyncRequest(req).setListener(new INKFAsyncRequestListener()
			{
				@Override
				public void receiveResponse(INKFResponseReadOnly aResponse, INKFRequestContext aContext) throws Exception
				{	//onRemoteDisconnect(aContext,instanceId,false);
					cleanup(aContext,instanceId);
				}
				
				@Override
				public void receiveException(NKFException aException, INKFRequest aRequest, INKFRequestContext aContext) throws Exception
				{	//onRemoteDisconnect(aContext,instanceId,false);
					cleanup(aContext,instanceId);
					throw aException;
				}
			});
		}
		else if (action.equals("agentDisconnect"))
		{	String instanceId=aContext.source("arg:instanceId",String.class);
			if (onRemoteDisconnect(aContext,instanceId))
			{	aContext.logRaw(INKFLocale.LEVEL_INFO, "Agent ["+instanceId+"] asked to disconnect");
			}
		}
		else if (action.equals("agentHold"))
		{	String instanceId=aContext.source("arg:instanceId",String.class);
			String remoteInfo=aContext.source("arg:remoteInfo",String.class);
			onRemoteConnect(aContext, instanceId,remoteInfo);
			aContext.setNoResponse();
		}
		else if (action.equals("agentList"))
		{	IHDSDocument agents=getAgentList();
			INKFResponse resp=aContext.createResponseFrom(agents);
			resp.setExpiry(INKFResponse.EXPIRY_MIN_FUNCTION_DEPENDENT,mAgentListExpiry);
		}
		else if (action.equals("agentError"))
		{	String instanceId=aContext.source("arg:instanceId",String.class);
			onAgentError(instanceId,aContext);
		}
	}
	
	private void onAgentError(String aInstanceId, INKFRequestContext aContext) throws Exception
	{
		String error=aContext.source("arg:error",String.class);
		String msg=String.format("Agent Error on [%s]: %s",aInstanceId,error);
		aContext.logRaw(INKFLocale.LEVEL_WARNING, msg);
		boolean removed=onRemoteDisconnect(aContext,aInstanceId);
		if (removed)
		{	aContext.logRaw(INKFLocale.LEVEL_WARNING, "Agent ["+aInstanceId+"] ejected after failure detected");
		}
	}
		
	private IHDSDocument getAgentList() throws Exception
	{	IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("agents");
		synchronized (mClients)
		{	for (Client agent : mClients.values())
			{	m.pushNode("agent")
				.addNode("id", agent.getId())
				.addNode("ip", agent.getIP())
				.addNode("connected", agent.isConnected())
				.addNode("lastConnect", agent.getLastConnect())
				.addNode("context", agent.getContext())
				.popNode();
			}
		}
		m.declareKey("byId", "/agents/agent", "id");
		return m.toDocument(false);
	}
	
	void cleanup(INKFRequestContext aContext, String aInstanceId) throws Exception
	{
		Client removed=null;
		synchronized (mClients)
		{	removed=mClients.get(aInstanceId);
		}
		if (removed!=null)
		{	removed.disconnect();
			onAgentListChanged(aContext);
		}
	}

	boolean onRemoteDisconnect(INKFRequestContext aContext, String aInstanceId) throws Exception
	{
		boolean result=false;
		Client removed=null;
		synchronized (mClients)
		{	removed=mClients.get(aInstanceId);
		}
		if (removed!=null)
		{	
			INKFRequestContext context=removed.mContext;
			if (context!=null && aContext!=context)
			{
				INKFResponse response=context.createResponseFrom("server disconnected");
				response.setExpiry(INKFResponse.EXPIRY_ALWAYS);
				IResponse kResp=((NKFResponseImpl)response).getKernelResponse();
				((NKFContextImpl)context).handleAsyncResponse(kResp);
				result=true;
			}
		}
		return result;
	}

	void onRemoteConnect(INKFRequestContext aContext, String aInstanceId, String aRemoteInfo) throws Exception
	{
		//format of remoteInfo from Netty is /[IP]:[PORT]
		int i=aRemoteInfo.indexOf(':');
		String ip=aRemoteInfo.substring(1,i);
		
		Client newClient=null;
		synchronized (mClients)
		{
			// TODO do we need to worry about stale/duplicate clients here? Will just warn for now
			if (mClients.containsKey(aInstanceId))
			{	aContext.logRaw(INKFLocale.LEVEL_WARNING, "Encountered existing agent for instance '" + aInstanceId + "'");
				onRemoteDisconnect(aContext,aInstanceId);
			}
			
		
			newClient = new Client(aContext, aInstanceId, ip);
			mClients.put(aInstanceId, newClient);
			
		}
		if (newClient!=null)
		{	
			aContext.logRaw(INKFLocale.LEVEL_INFO, "Agent connected ["+aInstanceId+"]");
			onAgentListChanged(aContext);
		}
	}
	
	private void onAgentListChanged(INKFRequestContext aContext) throws Exception
	{
		try
		{	cutExpiry();
			INKFRequest req=aContext.createRequest("active:agentUpdateAssignments");
			aContext.issueAsyncRequest(req);
		}
		catch (NKFException e)
		{	aContext.logRaw(INKFLocale.LEVEL_WARNING, "Unhanded error requesting active:agentListChanged\n"+Utils.throwableToString(e));
		}
	}
	
	
	protected static class Client
	{
		private INKFRequestContext mContext;
		private String mInstanceId;
		private final String mIP;
		private final long mLastConnect;

		public Client(INKFRequestContext aContext, String aInstanceId, String aIP) throws NKFException
		{
			mContext = aContext;
			mInstanceId = aInstanceId;
			mIP=aIP;
			mLastConnect=System.currentTimeMillis();
		}
		
		public boolean disconnect()
		{	boolean result=mContext!=null;
			mContext=null;
			return result;
		}
		
		
		public String getId()
		{
			return mInstanceId;
		}
		
		public String getIP()
		{
			return mIP;
		}
		
		public long getLastConnect()
		{
			return mLastConnect;
		}

		public INKFRequestContext getContext()
		{
			return mContext;
		}
		
		public boolean isConnected()
		{	return mContext!=null;
		}

		
	}

}
