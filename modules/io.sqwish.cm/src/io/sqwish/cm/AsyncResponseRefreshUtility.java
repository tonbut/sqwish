package io.sqwish.cm;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.netkernel.layer0.nkf.*;
import org.netkernel.layer0.util.GoldenThreadExpiryFunction;

/** Construct using a request, this utility will issue this request and return the response using the
 * getResponse() method. However unlike regular request issuing this utility will ensure only one request
 * is issued at once. On the first invocation additional callers will block, but subsequently they will
 * receive a cached response or a stale response until a new response is available, at which time the response will be invalidated.
 * If the cached response is expired it will still be returned until the response from an asynchronously issued request
 * is received, at which time all previously issued responses will be invalidated.
 * This ensures that minimal requests are issued and there is no delay in return response (once primed)
 */
public class AsyncResponseRefreshUtility implements INKFAsyncRequestListener
{
	private final INKFRequest mRequest;
	private INKFResponseReadOnly mLastResponse;
	private AtomicReference<GoldenThreadExpiryFunction> mPendingUpdateExpiry=new AtomicReference<>();
	private Semaphore mSubRequestSemaphore=new Semaphore(1);
	
	public AsyncResponseRefreshUtility(INKFRequest aRequest)
	{
		mRequest=aRequest;
		//mRequest.setHeader(INKFRequest.HEADER_EXCLUDE_DEPENDENCIES, Boolean.TRUE);
		mPendingUpdateExpiry.set(new GoldenThreadExpiryFunction("AsyncResponseRefreshUtility"));
	}
	
	public INKFResponse getResponse(INKFRequestContext aContext) throws Exception
	{
		INKFResponseReadOnly lastResponse=mLastResponse;
		INKFResponse result;
		if (lastResponse==null)
		{
			boolean acquired=false;
			while( !(acquired=mSubRequestSemaphore.tryAcquire(100, TimeUnit.MILLISECONDS)) )
			{	lastResponse=mLastResponse;
				if (lastResponse!=null) break;
			}
			if (acquired && (lastResponse==null || lastResponse.isExpired()) )
			{
				try
				{	lastResponse=setNewResponse(aContext.issueRequestForResponse(getRequest(aContext)));
				}
				finally
				{	mSubRequestSemaphore.release();
				}
			}
			result=aContext.createResponseFrom(lastResponse);
			/*
			if (lastResponse.isExpired())
			{	result.setExpiry(INKFResponse.EXPIRY_FUNCTION,mPendingUpdateExpiry.get());
			}
			*/
		}
		else
		{
			if (lastResponse.isExpired())
			{
				result=aContext.createResponseFrom(lastResponse);
				result.setExpiry(INKFResponse.EXPIRY_FUNCTION,mPendingUpdateExpiry.get());
				backgroundRefresh(aContext);
			}
			else
			{	
				result=aContext.createResponseFrom(lastResponse);
			}
		}
		//System.out.println(result.isExpired());
		return result;
	}
	
	private void backgroundRefresh(INKFRequestContext aContext) throws Exception
	{
		if (mSubRequestSemaphore.tryAcquire())
		{	try
			{	aContext.issueAsyncRequest(getRequest(aContext)).setListener(this);
			}
			catch(Exception e)
			{	mSubRequestSemaphore.release();
				throw e;
			}
		}
	}
	
	private INKFRequest getRequest(INKFRequestContext aContext) throws Exception
	{
		INKFRequest req=aContext.createRequest(mRequest.getIdentifier());
		return req;
	}
	
	private INKFResponseReadOnly setNewResponse(INKFResponseReadOnly aResponse)
	{
		mLastResponse=aResponse;
		GoldenThreadExpiryFunction old=mPendingUpdateExpiry.getAndSet(new GoldenThreadExpiryFunction("AsyncResponseRefreshUtility"));
		old.invalidate();
		return aResponse;
	}

	@Override
	public void receiveResponse(INKFResponseReadOnly aResponse, INKFRequestContext aContext) throws Exception
	{	
		setNewResponse(aResponse);
		mSubRequestSemaphore.release();
		aContext.setNoResponse();
	}

	@Override
	public void receiveException(NKFException aException, INKFRequest aRequest, INKFRequestContext aContext) throws Exception
	{
		INKFResponseReadOnly resp=aContext.createResponseFrom(aException);
		setNewResponse(resp);
		mSubRequestSemaphore.release();
		aContext.setNoResponse();
	}
}
