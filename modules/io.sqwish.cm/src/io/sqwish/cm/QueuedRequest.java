package io.sqwish.cm;

import org.netkernel.layer0.nkf.INKFRequestContext;

public class QueuedRequest
{
	private final String mNamespace;
	private final String mEndpointId;
	private final long mCreationTimestamp;
	private final Object mRequestBlob;
	private final INKFRequestContext mContextForResponse;
	
	public QueuedRequest(String aNamespace, String aEndpointId, long aCreationTimestamp, Object aRequestBlob, INKFRequestContext aContextForResponse)
	{
		mNamespace=aNamespace;
		mEndpointId=aEndpointId;
		mCreationTimestamp=aCreationTimestamp;
		mRequestBlob=aRequestBlob;
		mContextForResponse=aContextForResponse;
	}
	
	public String getNamespace()
	{	return mNamespace;
	}
	public String getEndpointId()
	{	return mEndpointId;
	}
	public long getCreationTimestamp()
	{	return mCreationTimestamp;
	}
	public Object getRequestBlob()
	{	return mRequestBlob;
	}
	public INKFRequestContext getContextForResponse()
	{	return mContextForResponse;
	}
	
}
