package io.sqwish.cm;

import java.util.HashMap;
import java.util.Map;

import org.netkernel.layer0.nkf.INKFAsyncRequestHandle;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class AgentStateAccessor extends StandardAccessorImpl
{
	public AgentStateAccessor()
	{	this.declareThreadSafe();
	}
		
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String agentId=aContext.getThisRequest().getArgumentValue("id");
		if (agentId!=null)
		{	onSingleState(agentId,aContext);
		}
		else
		{	onAllState(aContext);
		}
	}
	
	private void onSingleState(String aAgentId, INKFRequestContext aContext) throws Exception
	{
		IHDSReader agents=aContext.source("active:agentList",IHDSDocument.class).getReader();
		IHDSReader agent=agents.getFirstNodeOrNull("key('byId','"+aAgentId+"')");
		if (agent==null) throw new NKFException("Agent not found",aAgentId);
		
		INKFRequestContext agentContext=(INKFRequestContext)agent.getFirstValue("context");
		if (agentContext==null) throw new NKFException("Agent not connected",aAgentId);
		INKFRequest req=agentContext.createRequest("active:instanceInfo");
		req.setRepresentationClass(IHDSDocument.class);
		IHDSReader agentInfo=((IHDSDocument)agentContext.issueRequest(req)).getReader();
		
		INKFResponse resp=aContext.createResponseFrom(agentInfo.toDocument());
		resp.setExpiry(INKFResponse.EXPIRY_ALWAYS);
	}
	
	private void onAllState(INKFRequestContext aContext) throws Exception
	{
		IHDSReader agents=aContext.source("active:agentList",IHDSDocument.class).getReader();
		ManagerConfigRepresentation config=aContext.source("active:sqwishConfig",ManagerConfigRepresentation.class);
		
		Map<String, INKFAsyncRequestHandle> asyncRequests=new HashMap<>();
		for (IHDSReader agent : agents.getNodes("/agents/agent[connected='true']"))
		{
			String agentId=(String)agent.getFirstValue("id");
			INKFRequest req=aContext.createRequest("active:agentState");
			req.addArgument("id", (String)agentId);
			req.setRepresentationClass(IHDSDocument.class);
			INKFAsyncRequestHandle h=aContext.issueAsyncRequest(req);
			asyncRequests.put(agentId, h);
		}
		
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("agents");
		// TODO needs to be in configuration
		final long timeout=config.getAgentTimeout();
		
		for (Object agentIdRaw : agents.getValues("/agents/agent[connected='true']/id"))
		{	String agentId=(String)agentIdRaw;
			INKFAsyncRequestHandle h = asyncRequests.get(agentId);
			IHDSDocument agentState=(IHDSDocument)h.join(timeout);
			
			
			if (agentState==null)
			{	m.addNode("timeout", null);
				CMUtils.notifyAgentError(agentId,"timeout on instanceInfo request",aContext);
			}
			else
			{	m.pushNode("agent");
				m.addNode("id", agentId);
				m.appendChildren(agentState.getReader());
				m.popNode();
			}
			
		}
		m.declareKey("byId", "/agents/agent", "id");
		aContext.createResponseFrom(m.toDocument(false));
		
	}
}
