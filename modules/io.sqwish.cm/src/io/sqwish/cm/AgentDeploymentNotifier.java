package io.sqwish.cm;

import org.netkernel.layer0.nkf.INKFLocale;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.util.Utils;

public class AgentDeploymentNotifier extends StandardAccessorImpl
{
	public AgentDeploymentNotifier()
	{
		this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		ManagerConfigRepresentation config=aContext.source("active:sqwishConfig",ManagerConfigRepresentation.class);
		IHDSReader agentAssignments=aContext.source("active:agentAssignments",IHDSDocument.class).getReader();
		IHDSReader agentList=aContext.source("active:agentList",IHDSDocument.class).getReader();
		
		for (IHDSReader agent: agentList.getNodes("/agents/agent[connected='true']"))
		{
			String agentId=(String)agent.getFirstValue("id");
			INKFRequestContext agentContext=(INKFRequestContext)agent.getFirstValue("context");
			
			try
			{
				IHDSMutator m=HDSFactory.newDocument();
				m.pushNode("replicaSets");
				
				IHDSReader assignments=agentAssignments.getFirstNodeOrNull("key('agentById','"+agentId+"')");
				if (assignments!=null)
				{
					for (IHDSReader namespace : assignments.getNodes("namespace"))
					{
						String namespaceId=(String)namespace.getFirstValue(".");
						IHDSDocument replicaSet=config.getReplicaSetDefinitionForId(namespaceId);
					
						m.appendChildren(replicaSet.getReader());
					}
	
					INKFRequest req=agentContext.createRequest("active:sinkDeployment");
					req.addArgumentByValue("operand", m.toDocument(false));
					agentContext.issueRequest(req);
				}
			}
			catch (NKFException e)
			{
				String msg="Unhandled exception notifying agent ["+agentId+"] of deployment changes: "+e.getDeepestId();
				aContext.logRaw(INKFLocale.LEVEL_INFO, msg);
			}
		}
		
		aContext.createResponseFrom("done");
		
	}
}
