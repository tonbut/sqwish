package io.sqwish.cm;

import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class DownloadModuleAccessor extends StandardAccessorImpl
{
	public DownloadModuleAccessor()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String hash=aContext.getThisRequest().getArgumentValue("hash");
		//System.out.println("DownloadModuleAccessor "+hash);
		
		ManagerConfigRepresentation rep=aContext.source("active:sqwishConfig",ManagerConfigRepresentation.class);
		
		String source=rep.getSourceForHash(hash);
		INKFResponseReadOnly sourceResponse=aContext.sourceForResponse(source);
		
		aContext.createResponseFrom(sourceResponse);
	}
}
