package io.sqwish.cm.history;

import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class TestChart extends StandardAccessorImpl
{
	public TestChart()
	{
		this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("query")
		.addNode("start", "-240000")
		.addNode("period", "5000")
		.pushNode("value").addNode("namespace", "default").addNode("field", "completed").popNode();
		
		INKFRequest req=aContext.createRequest("active:historicalQuery");
		req.addArgumentByValue("operator", m.toDocument(false));
		req.setRepresentationClass(IHDSDocument.class);
		IHDSDocument resultSet=(IHDSDocument)aContext.issueRequest(req);
		
		m=HDSFactory.newDocument();
		m.pushNode("chart")
		.addNode("type", "TimeSeriesData")
		.addNode("legend", "false")
		.addNode("backgroundColor", "rgba(255,255,255,0)")
		.addNode("axisColor", "rgba(255,255,255,0)")
		.addNode("textColor", "#AAA")
		.addNode("width", "100")
		.addNode("height", "50")
		.addNode("xAxis", "t")
		.addNode("xAxisTicks", "none")
		.addNode("yAxisTicks", "range")
		.pushNode("dataSets")
		.pushNode("dataSet")
		.addNode("id", "v1")
		.addNode("type", "area")
		.addNode("interpolate", "basis")
		.addNode("fill", "rgba(64,0,255,0.25)")
		.addNode("stroke","rgba(64,0,255,1.0)" );
		
		req=aContext.createRequest("active:declarativeTimeseriesChart");
		req.addArgumentByValue("operator", m.toDocument(false));
		req.addArgumentByValue("operand", resultSet);
		INKFResponseReadOnly respIn=aContext.issueRequestForResponse(req);
		aContext.createResponseFrom(respIn);
		
	}
}
