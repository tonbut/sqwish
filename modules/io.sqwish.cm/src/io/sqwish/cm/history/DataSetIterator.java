package io.sqwish.cm.history;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;

import org.netkernel.mod.hds.IHDSDocument;

public class DataSetIterator
{
	private final SortedMap<Long,IHDSDocument> mDataSet;
	private final String mQuery;
	private final long mStart;
	private Iterator<Entry<Long,IHDSDocument>> mIterator;
	private Entry<Long,IHDSDocument> mCurrent;
	private boolean mFirst=true;
	
	public DataSetIterator(SortedMap<Long,IHDSDocument> aDataSet, String aQuery, long aStart)
	{
		mDataSet=aDataSet;
		mQuery=aQuery;
		mStart=aStart;
		mIterator=mDataSet.entrySet().iterator();
	}
	
	public boolean next()
	{	
		if (mFirst)
		{	mCurrent=new Map.Entry<Long, IHDSDocument>()
			{
				public Long getKey()
				{
					return mStart;
				}

				@Override
				public IHDSDocument getValue()
				{
					return null;
				}

				@Override
				public IHDSDocument setValue(IHDSDocument value)
				{
					// TODO Auto-generated method stub
					return null;
				}
			};
			mFirst=false;
			return true;
		}
		else if (mIterator.hasNext())
		{	mCurrent=mIterator.next();
			return true;
		}
		return false;
	}
	
	public Long getTime()
	{	return mCurrent.getKey();
	}
	
	public Object getValue()
	{	return mCurrent.getValue()==null?null:mCurrent.getValue().getReader().getFirstValueOrNull(mQuery);
	}
}
