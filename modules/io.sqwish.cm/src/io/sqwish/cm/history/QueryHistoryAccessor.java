package io.sqwish.cm.history;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

import io.sqwish.cm.history.QueryIteratorController.IQueryIteratorController;

public class QueryHistoryAccessor extends StandardAccessorImpl
{
	
	public QueryHistoryAccessor()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		IHDSReader operator=aContext.source("arg:operator",IHDSDocument.class).getReader().getFirstNode("/*");
		long now=System.currentTimeMillis();
		long start=Long.parseLong((String)operator.getFirstValue("start"));
		long period=10000L;
		SortedMap<Long,IHDSDocument> dataset=PersistHistoryAccessor.getInstance().getDataSet(now+start, now);
		//System.out.println(dataset.size());
		long periodCount=-start/period;
		long endTime=now;
		long startTime=now-period*periodCount;
		try
		{	endTime=dataset.lastKey();
			startTime=endTime-period*periodCount;
		}
		catch (NoSuchElementException e)
		{ /* ignore */
		}
		
		List<QueryResultSet> resultSets=new ArrayList<QueryResultSet>();
		for (IHDSReader value : operator.getNodes("value"))
		{
			String namespace=(String)value.getFirstValueOrNull("namespace");
			String agent=(String)value.getFirstValueOrNull("agent");
			String client=(String)value.getFirstValueOrNull("client");
			String cm=(String)value.getFirstValueOrNull("cm");
			String field=(String)value.getFirstValueOrNull("field");
			String op=(String)value.getFirstValueOrNull("op");
			if (op==null) op="";
			String path;
			if (namespace!=null)
			{	path=String.format("/state/namespaces/namespace[id='%s']/%s",namespace,field);
			}
			else if (agent!=null)
			{	
				if (agent.equals(""))
				{	path=String.format("/state/agents/%s",field);
				}
				else
				{	path=String.format("/state/agents/agent[id='%s']/%s",agent,field);
				}
			}
			else if (client!=null)
			{	
				if (client.equals(""))
				{	path=String.format("/state/clients/%s",field);
				}
				else
				{	path=String.format("/state/clients/client[id='%s']/%s",client,field);
				}
			}
			else if (cm!=null)
			{	
				path=String.format("/state/cm/%s",field);
			}
			else
			{	continue;
			}
			
			
			
			IQueryIteratorController qic;
			switch (op) {
			case "diff":
				qic=QueryIteratorController.getDiffInstance();
				break;
			case "sample":
				qic=QueryIteratorController.getLastValueInstance();
				break;
			case "average":
			default:
				qic=QueryIteratorController.getAverageInstance();
				break;
			}
			
			
			DataSetIterator dsi=new DataSetIterator(dataset, path, startTime);
			
			QueryResultSet qrs=iterateForward(dsi,qic,startTime,endTime,period);
			resultSets.add(qrs);
		}
		
		IHDSMutator m=HDSFactory.newDocument().pushNode("data");
		if (resultSets.size()>0)
		{
			DateFormat df=new SimpleDateFormat("HH:mm");
			QueryResultSet indexSet=resultSets.get(0);
			for (int i=0; i<indexSet.size(); i++)
			{
				m.pushNode("row");
				long t=indexSet.getTimestamp(i);
				m.addNode("ts", t);
				m.addNode("t", df.format(new Date(t)));
				
				for (int j=0; j<resultSets.size(); j++)
				{
					Object v=resultSets.get(j).getValue(i);
					m.addNode("v"+(j+1),v);
				}
				m.popNode();
			}
		}
		
		INKFResponse resp=aContext.createResponseFrom(m.toDocument(false));
		resp.setExpiry(INKFResponse.EXPIRY_ALWAYS);
		
	}
	
	
	private static QueryResultSet iterateForward(DataSetIterator aIterator, IQueryIteratorController aController, long aStart, long aEnd, long aPeriod) throws NKFException
	{	Object lastValue=null;
		long lastTime=aStart;
		if (aIterator.next())
		{	lastValue=aIterator.getValue();
		}
		long now=System.currentTimeMillis();

		int index=0;
		boolean continueIterating;
		boolean useCurrent=false;
		
		List<Long> times=new ArrayList<Long>();
		List<Object> values=new ArrayList<Object>();
		long period=(aPeriod==0)?(aEnd-aStart):aPeriod;
		
		for (long t=aStart; t<aEnd; t+=period)
		{	long sampleEndTime=t+period;
			continueIterating=true;
			boolean lastSample=t+period>=aEnd;
			do
			{	if (!useCurrent)
				{	if (!aIterator.next())
					{	
						aController.accept(lastValue, lastTime, sampleEndTime-lastTime, index);
						lastTime=sampleEndTime;
						break;
					}
				}
				long time=aIterator.getTime();
				Object value=aIterator.getValue();
				if (time<sampleEndTime)
				{	if (continueIterating)
					{	long period2=time-lastTime;
						if (period2>0)
						{	continueIterating=aController.accept(lastValue, lastTime, period2, index);
						}
						index++;
						lastTime=time;
						lastValue=value;
					}
					useCurrent=false;
				}
				else
				{	//finish last segment
					aController.accept(lastValue, lastTime, sampleEndTime-lastTime, index);
					useCurrent=true;
					lastTime=sampleEndTime;
					break; //move into next sample
				}
			} while(continueIterating || !lastSample);
			
			if (sampleEndTime>now)
			{	
				values.add(null);
			}
			else
			{	values.add(aController.getResult());
			}
			times.add(t);
			index=0;
		}
		return new QueryResultSet(times,values);
	}
	
	
}
