package io.sqwish.cm.history;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class PersistHistoryAccessor extends StandardAccessorImpl
{
	private SortedMap<Long,IHDSDocument> mTimeseries=new TreeMap<>();
	private static PersistHistoryAccessor sInstance;
	
	public static PersistHistoryAccessor getInstance()
	{	return sInstance;
	}
	
	public PersistHistoryAccessor()
	{	this.declareThreadSafe();
		sInstance=this;
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		IHDSDocument operand=aContext.source("arg:operand",IHDSDocument.class);
		Long now=System.currentTimeMillis();
		mTimeseries.put(now, operand);
		
		long oldest=now-1000L*60*6L;
		SortedMap<Long,IHDSDocument> tail=mTimeseries.headMap(oldest);
		List<Long> toRemove=new ArrayList<>(tail.keySet());
		//System.out.println("tail="+toRemove.size()+" remaining="+mTimeseries.size());
		for (Long time : toRemove)
		{	mTimeseries.remove(time);
		}
	}
	
	public SortedMap<Long,IHDSDocument> getDataSet(long aStart, long aEnd)
	{
		return mTimeseries.subMap(aStart, aEnd);
	}
}
