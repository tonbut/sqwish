package io.sqwish.cm.history;

import java.util.List;


public class QueryResultSet
{
	private final List<Long> mTimes;
	private final List<Object> mValues;
	
	public QueryResultSet(List<Long> aTimes, List<Object> aValues)
	{	mTimes=aTimes;
		mValues=aValues;
	}

	public int size()
	{	return mTimes.size();
	}

	public long getTimestamp(int aIndex)
	{	return mTimes.get(aIndex);
	}

	public Object getValue(int aIndex)
	{	return mValues.get(aIndex);
	}
	
	public String toString()
	{
		StringBuilder sb=new StringBuilder();
		for (int i=0; i<size(); i++)
		{
			sb.append(getTimestamp(i));
			sb.append(" ");
			sb.append(getValue(i));
			sb.append("\n");
		}
		return sb.toString();
	}
}
