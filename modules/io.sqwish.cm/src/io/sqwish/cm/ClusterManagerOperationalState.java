package io.sqwish.cm;

import java.util.concurrent.atomic.AtomicReference;

import org.netkernel.layer0.nkf.INKFAsyncRequestHandle;
import org.netkernel.layer0.nkf.INKFLocale;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.urii.ParsedIdentifierImpl;
import org.netkernel.layer0.util.GoldenThreadExpiryFunction;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.util.Utils;

public class ClusterManagerOperationalState extends StandardAccessorImpl
{
	private boolean mIngress=true;
	private boolean mEgress=true;
	private AtomicReference<GoldenThreadExpiryFunction> mStateExpiry=new AtomicReference<>();
	
	public ClusterManagerOperationalState()
	{	this.declareThreadSafe();
		invalidateState();
	}
	
	private void invalidateState()
	{	GoldenThreadExpiryFunction newExpiry=new GoldenThreadExpiryFunction("ClusterManagerOperationalState");
		GoldenThreadExpiryFunction lastExpiry=mStateExpiry.getAndSet(newExpiry);
		if (lastExpiry!=null) lastExpiry.invalidate();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String type=aContext.getThisRequest().getArgumentValue(ParsedIdentifierImpl.ARG_ACTIVE_TYPE);
		if (type.equals("cmState"))
		{	onSourceInner(aContext);
		}
		else if (type.equals("cmState-Flush"))
		{	onFlush(aContext);
		}
		else if (type.equals("cmState-Ingress-Enable"))
		{	mIngress=true;
			aContext.logRaw(INKFLocale.LEVEL_INFO, "Ingress Enabled");
			invalidateState();
		}
		else if (type.equals("cmState-Ingress-Disable"))
		{	mIngress=false;
			aContext.logRaw(INKFLocale.LEVEL_INFO, "Ingress Disabled");
			invalidateState();
		}
		else if (type.equals("cmState-Egress-Enable"))
		{	mEgress=true;
			aContext.logRaw(INKFLocale.LEVEL_INFO, "Egress Enabled");
			invalidateState();
		}
		else if (type.equals("cmState-Egress-Disable"))
		{	mEgress=false;
			aContext.logRaw(INKFLocale.LEVEL_INFO, "Egress Disabled");
			invalidateState();
		}
	}
	
	public void onSourceInner(INKFRequestContext aContext) throws Exception
	{
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("state")
		.addNode("ingress", mIngress)
		.addNode("egress", mEgress);
		INKFResponse resp=aContext.createResponseFrom(m.toDocument(false));
		resp.setExpiry(INKFResponse.EXPIRY_FUNCTION,mStateExpiry.get());
	}
	
	public void onFlush(INKFRequestContext aContext) throws Exception
	{
		//disable ingress/egress
		//flush each agent
		//flush queue
		//enable egress/ingress
		ManagerConfigRepresentation config=aContext.source("active:sqwishConfig",ManagerConfigRepresentation.class);
		long timeout=config.getAgentTimeout();
		
		
		//flush CM queue
		Integer queueFlushCount=aContext.source("active:routeClientRequestFlush",Integer.class);
		
		//flush agents
		int agentFlushCount=0;
		IHDSReader agentList=aContext.source("active:agentList",IHDSDocument.class).getReader();
		for (IHDSReader agent : agentList.getNodes("/agents/agent"))
		{
			String agentId=(String)agent.getFirstValueOrNull("id");
			INKFRequestContext agentContext=(INKFRequestContext)agent.getFirstValueOrNull("context");
			if (agentContext!=null)
			{
				INKFRequest req=agentContext.createRequest("active:flushRequests");
				req.setRepresentationClass(Integer.class);
				try
				{	
					INKFAsyncRequestHandle handle=agentContext.issueAsyncRequest(req);
					Integer flushCount=((Integer)handle.join(timeout));
					if (flushCount!=null)
					{
						String msg=String.format("Flushed %d pending requests on [%s]", flushCount, agentId);
						aContext.logRaw(INKFLocale.LEVEL_INFO, msg);
						agentFlushCount+=flushCount;
					}
					else
					{	CMUtils.notifyAgentError(agentId,"timeout on flushRequests request",aContext);
					}
				}
				catch (Exception e)
				{
					aContext.logRaw(INKFLocale.LEVEL_WARNING, "Unhandled exception in active:flushRequests to "+agentId+":\n"+Utils.throwableToString(e));
				}
			}
		}
		
		String msg=String.format("Flushed %d requests from queue, %d requests from agents",queueFlushCount,agentFlushCount);
		aContext.logRaw(INKFLocale.LEVEL_INFO, msg);
		
	}
}
