package io.sqwish.cm;

import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.urii.ParsedIdentifierImpl;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class ClientRequestRouterStatus extends StandardAccessorImpl
{
	public ClientRequestRouterStatus()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String type=aContext.getThisRequest().getArgumentValue(ParsedIdentifierImpl.ARG_ACTIVE_TYPE);
		if (type.equals("routeClientRequestStatus"))
		{	onStatus(aContext);
		}
		else if (type.equals("routeClientRequestFlush"))
		{	onFlush(aContext);
		}
	}
	
	public void onStatus(INKFRequestContext aContext) throws Exception
	{
		ClientRequestRouter router=ClientRequestRouter.getInstance();
		IHDSDocument status=router.getStatus();
		INKFResponse resp=aContext.createResponseFrom(status);
		resp.setExpiry(INKFResponse.EXPIRY_ALWAYS);
	}
	
	public void onFlush(INKFRequestContext aContext) throws Exception
	{
		ClientRequestRouter router=ClientRequestRouter.getInstance();
		int count=router.flush();
		aContext.createResponseFrom(count).setExpiry(INKFResponse.EXPIRY_ALWAYS);
	}
}
