package io.sqwish.cm;

import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class NKPConfigAccessor extends StandardAccessorImpl
{
	public NKPConfigAccessor()
	{
		this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		ManagerConfigRepresentation config=aContext.source("active:sqwishConfig",ManagerConfigRepresentation.class);
		
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("config")
		.pushNode("tunnel")
		.addNode("@factory", "com.ten60.netkernel.nkp.netty.NettyNKPTunnelFactory")
		.addNode("port", "32768")
		.addNode("keepAlive", "true");
		
		
		if (config.isTLS())
		{	m.addNode("tls", "true")
			.addNode("tlsKeyStore", config.getTLSKeystore())
			.addNode("tlsKeyStorePassword",config.getTLSKeystorePassword())
			.addNode("tlsKeyPassword",config.getTLSKeyPassword());
		}
		
		m.popNode()
		.addNode("enabled", "true")
		.addNode("passByValue", "true")
		.addNode("killPending", "true")
		.pushNode("authenticationRequest")
		.addNode("identifier","active:authenticate")
		.pushNode("argument", "arg:username").addNode("@name", "username").popNode()
		.pushNode("argument", "arg:password").addNode("@name", "password").popNode()
		.popNode();
		
		aContext.createResponseFrom(m.toDocument(false));
	}
}
