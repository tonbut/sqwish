package io.sqwish.cm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.netkernel.layer0.nkf.INKFAsyncRequestHandle;
import org.netkernel.layer0.nkf.INKFExpiryFunction;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.layer0.urii.ParsedIdentifierImpl;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class AgentDeploymentStateAccessor extends StandardAccessorImpl
{
	public AgentDeploymentStateAccessor()
	{	this.declareThreadSafe();
	}
	
	@Override
	protected void postCommission(INKFRequestContext aContext) throws Exception
	{
		INKFRequest req=aContext.createRequest("active:agentDeploymentStateInner");
		mARFU=new AsyncResponseRefreshUtility(req);
	}
		
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String agentId=aContext.getThisRequest().getArgumentValue("id");
		if (agentId!=null)
		{	onSingleDeployment(agentId,aContext);
		}
		else
		{	
			onAllDeploymentInner(aContext);
			/*
			String type=aContext.getThisRequest().getArgumentValue(ParsedIdentifierImpl.ARG_ACTIVE_TYPE);
			if (type.equals("agentDeploymentStateInner"))
			{	onAllDeploymentInner(aContext);
			}
			else
			{	onAllDeployment(aContext);
			}
			*/
		}
	}
	
	private AsyncResponseRefreshUtility mARFU;
	
	private void onAllDeployment(INKFRequestContext aContext) throws Exception
	{
		
		System.out.println("onAllDeployment");
		mARFU.getResponse(aContext);
		/*
			INKFRequest req=aContext.createRequest("active:agentDeploymentStateInner");
			req.setRepresentationClass(IHDSDocument.class);
			INKFResponseReadOnly<IHDSDocument> respIn=aContext.issueRequestForResponse(req);
			mAllDeploymentResponse=respIn;
			aContext.createResponseFrom(respIn);
			*/

	}
	
	private void onAllDeploymentInner(INKFRequestContext aContext) throws Exception
	{
		IHDSReader agents=aContext.source("active:agentList",IHDSDocument.class).getReader();
		ManagerConfigRepresentation config=aContext.source("active:sqwishConfig",ManagerConfigRepresentation.class);
		
		
		Map<String, INKFAsyncRequestHandle> asyncRequests=new HashMap<>();
		List<Object> agentIds=agents.getValues("/agents/agent[connected='true']/id");
		
		for (Object agentIdRaw : agentIds)
		{		
			String agentId=(String)agentIdRaw;
			INKFRequest req=aContext.createRequest("active:agentDeploymentState");
			req.addArgument("id", agentId);
			req.setRepresentationClass(IHDSDocument.class);
			INKFAsyncRequestHandle h=aContext.issueAsyncRequest(req);
			asyncRequests.put(agentId, h);
		}
		
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("agents");
		// TODO needs to be in configuration
		final long timeout=config.getAgentTimeout();
			
		for (Object agentIdRaw : agentIds)
		{	String agentId=(String)agentIdRaw;
			INKFAsyncRequestHandle h = asyncRequests.get(agentId);
			IHDSDocument agentState=(IHDSDocument)h.join(timeout);
			
			
			if (agentState==null)
			{	m.addNode("timeout", null);
				CMUtils.notifyAgentError(agentId,"timeout on deployedModulesList request",aContext);
			}
			else
			{
				m.pushNode("agent");
				m.addNode("id", agentId);
				m.appendChildren(agentState.getReader());
				m.popNode();
			}
			
		}
		m.declareKey("byId", "/agents/agent", "id");
		aContext.createResponseFrom(m.toDocument(false));	
	}
	
	private void onSingleDeployment(String aAgentId, INKFRequestContext aContext) throws Exception
	{
		IHDSReader agents=aContext.source("active:agentList",IHDSDocument.class).getReader();
		IHDSReader agent=agents.getFirstNodeOrNull("key('byId','"+aAgentId+"')");
		if (agent==null) throw new NKFException("Agent not found",aAgentId);
		INKFRequestContext agentContext=(INKFRequestContext)agent.getFirstValue("context");
		if (agentContext==null) throw new NKFException("Agent not connected",aAgentId);
		
		IHDSReader agentAssignments=aContext.source("active:agentAssignments",IHDSDocument.class).getReader();
		ManagerConfigRepresentation config=aContext.source("active:sqwishConfig",ManagerConfigRepresentation.class);
		
		
		INKFRequest req=agentContext.createRequest("active:deployedModulesList");
		req.setRepresentationClass(IHDSDocument.class);
		//long t0=System.currentTimeMillis();
		IHDSReader agentDeployment=((IHDSDocument)agentContext.issueRequest(req)).getReader();
		//System.out.println("deployedModulesList "+aAgentId+" "+(System.currentTimeMillis()-t0));
		
		Map<String,IHDSReader> expectedModulesForAgent=new HashMap<>();
		IHDSReader assignments=agentAssignments.getFirstNodeOrNull("key('agentById','"+aAgentId+"')");
		if (assignments!=null)
		{
			for (IHDSReader namespace : assignments.getNodes("namespace"))
			{
				String namespaceId=(String)namespace.getFirstValue(".");
				IHDSDocument replicaSet=config.getReplicaSetDefinitionForId(namespaceId);
				//System.out.println(replicaSet);
				for (IHDSReader module : replicaSet.getReader().getNodes("/replicaSet/module"))
				{	String id=(String)module.getFirstValue("id");
					expectedModulesForAgent.put(id,module);
				}
			}
		}
		
		//work out difference (is same for now)
		//all modules deployed, no extra ones
		boolean mismatch=false;
		for (IHDSReader module : agentDeployment.getNodes("/modules/module"))
		{
			String moduleURI=(String)module.getFirstValue("id");
			String moduleVersion=(String)module.getFirstValue("version");
			String actualContentMD5=(String)module.getFirstValue("content_md5");
			int actualHash=(moduleURI+moduleVersion).hashCode();
			String actualHashString=String.format("%08X",actualHash);
			IHDSReader expectedModule=expectedModulesForAgent.remove(actualHashString);
			if (expectedModule!=null)
			{	String expectedContentMD5=(String)expectedModule.getFirstValue("content_md5");
				if (!expectedContentMD5.equals(actualContentMD5))
				{	mismatch=true;
					break;
				}
			}
			else
			{	mismatch=true;
				break;
			}
		}
		//check for no extra modules
		if (!mismatch)
		{	mismatch=expectedModulesForAgent.size()>0;
		}
		
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("deployment");
		m.addNode("correct", !mismatch);
		aContext.createResponseFrom(m.toDocument(false));
	}
	
}
