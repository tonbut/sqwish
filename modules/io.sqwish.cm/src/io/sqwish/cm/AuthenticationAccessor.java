package io.sqwish.cm;

import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class AuthenticationAccessor extends StandardAccessorImpl
{
	public AuthenticationAccessor()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String username=aContext.source("arg:username",String.class);
		String password=aContext.source("arg:password",String.class);
		
		ManagerConfigRepresentation config=aContext.source("active:sqwishConfig",ManagerConfigRepresentation.class);
		
		String passwordHash=config.getPasswordHashFor(username);
		boolean result;
		if (passwordHash!=null)
		{
			INKFRequest req=aContext.createRequest("active:checkPasswordHash");
			req.addArgumentByValue("password", password);
			req.addArgumentByValue("hash", passwordHash);
			req.setRepresentationClass(Boolean.class);
			result=(Boolean)aContext.issueRequest(req);
		}
		else
		{	result=false;
		}
		aContext.createResponseFrom(result);
	}
}
