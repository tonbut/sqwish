package io.sqwish.cm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.netkernel.layer0.meta.IEndpointStateMeta;
import org.netkernel.layer0.meta.ISpaceElements;
import org.netkernel.layer0.meta.ISpaceMeta;
import org.netkernel.layer0.nkf.INKFAsyncRequestListener;
import org.netkernel.layer0.nkf.INKFLocale;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.layer0.nkf.impl.NKFContextImpl;
import org.netkernel.layer0.nkf.impl.NKFLifecycleEventContextImpl;
import org.netkernel.layer0.nkf.impl.NKFResponseImpl;
import org.netkernel.layer0.nkf.impl.NKFResponseReadOnlyImpl;
import org.netkernel.layer0.representation.IHDSNode;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.request.IExpiry;
import org.netkernel.request.IRequest;
import org.netkernel.request.IResponse;
import org.netkernel.urii.IEndpoint;
import org.netkernel.urii.IMetaRepresentation;
import org.netkernel.util.Utils;

import com.sun.org.apache.bcel.internal.generic.NEW;

public class ClientRequestRouter extends StandardAccessorImpl 
{
	private static ClientRequestRouter sInstance;
	private Timer mTimer;
	private long mAgentTimeout=1000;
	private INKFRequestContext mContext;
	private INKFResponseReadOnly<ManagerConfigRepresentation> mLastConfigResponse;
	private AtomicBoolean mUpdateConfigLock=new AtomicBoolean();
	private INKFResponseReadOnly<IHDSDocument> mLastRoutingTableResponse;
	private AtomicBoolean mUpdateOperationalStateLock=new AtomicBoolean();
	private INKFResponseReadOnly<IHDSDocument> mLastOperationalStateResponse;
	private AtomicBoolean mUpdateRoutingTableLock=new AtomicBoolean();
	private boolean mIngress;
	private boolean mEgress;
	private ConcurrentMap<String, AgentLiveState> mAgentMap=new ConcurrentHashMap();
	private ConcurrentMap<String, NamespaceLiveState> mNamespaceMap=new ConcurrentHashMap();
	private Map<String, ClientLiveState> mClientMap = new ConcurrentHashMap<String, ClientRequestRouter.ClientLiveState>();
	private IEndpointStateMeta mNKPServerInstance;
	private boolean mStopped=false;
	
	public static ClientRequestRouter getInstance()
	{	return sInstance;
	}
	
	public ClientRequestRouter()
	{	sInstance=this;
		this.declareThreadSafe();
		mTimer=new Timer();
	}
	
	private class ServiceQueueTask extends TimerTask
	{	public void run()
		{	
			try
			{	ensureOperationalState(mContext);
			}
			catch (Exception e)
			{
				mContext.logRaw(INKFLocale.LEVEL_WARNING, Utils.throwableToString(e));
			}
			while(serviceQueue(mContext)) {;}
		}
	}
	
	private class CheckClientStatusTask extends TimerTask
	{	public void run()
		{
			if (mNKPServerInstance!=null)
			{
				try
				{	
					//find all clients that are no long registered
					Set<String> clients=new HashSet<>(mClientMap.keySet());
					IHDSNode connectionState=(IHDSNode)mNKPServerInstance.getState().getValue("connection");
					for (Object remoteInfoRaw : connectionState.getValues("/nkp/remote/connection"))
					{	clients.remove(remoteInfoRaw);
					}
					
					//remove the clients that are no longer connected
					for (String clientKey : clients)
					{	
						String msg=String.format("Client [%s] disconnected",clientKey);
						mContext.logRaw(INKFLocale.LEVEL_INFO, msg);
						mClientMap.remove(clientKey);
					}
					
				} catch (Exception e)
				{	mContext.logRaw(INKFLocale.LEVEL_WARNING, "Unhandled exception obtaining metadata for NKPServer:\n"+Utils.throwableToString(e));
				}
				
				
				
			}
		}
	}
	
	@Override
	protected void postCommission(INKFRequestContext aContext) throws Exception
	{
		super.postCommission(aContext);
		mContext=new NKFLifecycleEventContextImpl(aContext.getKernelContext().getKernel(), aContext.getKernelContext().getRequestScope(), this, null);
		
		//get NKPServer for metadata
		try
		{	
			IMetaRepresentation rep=mContext.meta("");
			ISpaceMeta sm=(ISpaceMeta)rep;
			ISpaceElements elements=sm.getElements();
			for (int i=0; i<elements.size(); i++)
			{	
				IEndpoint ep=elements.getPhysicalEndpoint(i);
				if (ep.getClass().getName().endsWith("NKPServerTransport"))
				{	mNKPServerInstance=(IEndpointStateMeta)ep;
					break;
				}
			}
		} catch (Exception e)
		{	mContext.logRaw(INKFLocale.LEVEL_WARNING, "Unhandled exception obtaining NKPServer instance:\n"+Utils.throwableToString(e));
		}
		
		TimerTask serviceQueue=new ServiceQueueTask();
		mTimer.schedule(serviceQueue, 100L, 100L);
		
		TimerTask checkClientStatus=new CheckClientStatusTask();
		mTimer.schedule(checkClientStatus, 5000L, 5000L);
		
		
	}
	
	@Override
	public void preDecommission(INKFRequestContext aContext) throws Exception
	{	mStopped=true;
		mTimer.cancel();
	}
	
	private static class ClientLiveState
	{
		private final String mId;
		private final String mIP;
		private final String mPort;
		private final String mNamespace;
		private final AtomicLong mReceivedRequests=new AtomicLong();
		private long mLastCount=-1;
		private long mLastTime=0;
		
		public ClientLiveState(String aId, String aIP, String aPort, String aNamespace)
		{	this.mId=aId;
			this.mIP = aIP;
			this.mPort = aPort;
			this.mNamespace = aNamespace;
		}
		public String getId()
		{	return mId;
		}
		
		public String getIP()
		{	return mIP;
		}

		public String getPort()
		{	return mPort;
		}

		public String getNamespace()
		{	return mNamespace;
		}
		
		public long getReceivedRequestCount()
		{	return mReceivedRequests.get();
		}
		
		public void incrementReceivedRequests()
		{	mReceivedRequests.incrementAndGet();
		}
		
		public float getReceivedRequestRate(long aNow)
		{
			float result;
			long count=getReceivedRequestCount();
			if (mLastTime>0)
			{	
				long tdiff=aNow-mLastTime;
				long cdiff=count-mLastCount;
				result=((float)cdiff*1000.0f)/((float)tdiff);
			}
			else
			{	result=0.0f;
			}
			mLastTime=aNow;
			mLastCount=count;
			return result;
		}
	}
	
	private void ensureConfig(INKFRequestContext aContext) throws Exception
	{
		if (mLastConfigResponse==null || mLastConfigResponse.isExpired())
		{	if (mUpdateConfigLock.compareAndSet(false, true))
			{	try
				{	INKFRequest req=aContext.createRequest("active:sqwishConfig");
					req.setRepresentationClass(ManagerConfigRepresentation.class);
					req.setHeader(IRequest.HEADER_EXCLUDE_DEPENDENCIES, true);
					mLastConfigResponse=aContext.issueRequestForResponse(req);
					ManagerConfigRepresentation config=mLastConfigResponse.getRepresentation();
					mAgentTimeout=config.getAgentTimeout();
				}
				finally
				{	mUpdateConfigLock.set(false);
				}
			}
		}
	}
	
	private void ensureOperationalState(INKFRequestContext aContext) throws Exception
	{
		if (mLastOperationalStateResponse==null || mLastOperationalStateResponse.isExpired())
		{	if (mUpdateOperationalStateLock.compareAndSet(false, true))
			{	try
				{	INKFRequest req=aContext.createRequest("active:cmState");
					req.setRepresentationClass(IHDSDocument.class);
					req.setHeader(IRequest.HEADER_EXCLUDE_DEPENDENCIES, true);
					mLastOperationalStateResponse=aContext.issueRequestForResponse(req);
					IHDSReader r=mLastOperationalStateResponse.getRepresentation().getReader().getFirstNode("*");
					mIngress=(Boolean)r.getFirstValue("ingress");
					mEgress=(Boolean)r.getFirstValue("egress");
				}
				finally
				{	mUpdateOperationalStateLock.set(false);
				}
			}
		}
	}
	
	private IHDSReader getRoutingTable(INKFRequestContext aContext) throws Exception
	{
		if (mLastRoutingTableResponse==null || mLastRoutingTableResponse.isExpired())
		{	if (mUpdateRoutingTableLock.compareAndSet(false, true))
			{	try
				{	INKFRequest req=aContext.createRequest("active:routingTable");
					req.setRepresentationClass(IHDSDocument.class);
					req.setHeader(IRequest.HEADER_EXCLUDE_DEPENDENCIES, true);
					mLastRoutingTableResponse=aContext.issueRequestForResponse(req);
				}
				finally
				{	mUpdateRoutingTableLock.set(false);
				}
			}
			else
			{	return null;
			}
		}
		return mLastRoutingTableResponse.getRepresentation().getReader();
	}
	
	private static class NamespaceLiveState
	{
		private final String mName;
		private final AtomicLong mAcceptedRequestCount=new AtomicLong();
		private final AtomicLong mExecutedRequestCount=new AtomicLong();
		private final AtomicLong mCompletedRequestCount=new AtomicLong();
		private final Deque<QueuedRequest> mQueue;
		private final ConcurrentHashMap<String, EndpointLiveState> mEndpointState;
		
		public NamespaceLiveState(String aName)
		{	mName=aName;
			mQueue=new ConcurrentLinkedDeque<QueuedRequest>();
			mEndpointState=new ConcurrentHashMap<>();
		}
		public String getName()
		{	return mName;
		}
		public long getAcceptedRequestCount()
		{	return mAcceptedRequestCount.get();
		}
		public long getExecutedRequestCount()
		{	return mExecutedRequestCount.get();
		}
		public long getCompletedRequestCount()
		{	return mCompletedRequestCount.get();
		}
		public void incrementAcceptedCount()
		{	mAcceptedRequestCount.incrementAndGet();
		}
		public void incrementExecCount()
		{	mExecutedRequestCount.incrementAndGet();
		}
		public void decrementExecCount()
		{	mExecutedRequestCount.decrementAndGet();
		}
		public void incrementCompleteCount()
		{	mCompletedRequestCount.incrementAndGet();
		}
		public Deque<QueuedRequest> getQueue()
		{	return mQueue;
		}
		
		private final AtomicLong mQueueTime=new AtomicLong();
		private final AtomicLong mQueueCount=new AtomicLong();
		
		public long getAverageQueueWait()
		{
			long totalTime=mQueueTime.getAndSet(0L);
			long count=mQueueCount.getAndSet(0L);
			if (count>0)
			{	long lastAverage=totalTime/count;
				return lastAverage;
			}
			else
			{	return 0L;
			}
		}
		
		public void declareQueueWait(long aResponseTime)
		{
			mQueueTime.addAndGet(aResponseTime);
			mQueueCount.incrementAndGet();
		}
		
		public void declareExecutionTime(String aEndpointId, long aExecutionTime)
		{
			//System.out.println("response time "+aEndpointId+" "+aResponseTime);
			EndpointLiveState els=mEndpointState.get(aEndpointId);
			if (els==null)
			{	
				EndpointLiveState newState=new EndpointLiveState(aEndpointId);
				EndpointLiveState existing=mEndpointState.putIfAbsent(aEndpointId, newState);
				if (existing==null)
				{	els=newState;
				}
				else
				{	els=existing;
				}
			}
			els.declareResponseTime(aExecutionTime);
		}
		
		public long getAverageResponseTime(String aEndpointId)
		{	EndpointLiveState els=mEndpointState.get(aEndpointId);
			if (els!=null)
			{	return els.getAverageResponseTime();
			}
			else
			{	return 0;
			}
		}
		
		public Collection<EndpointLiveState> getEndpoints()
		{	return new ArrayList<EndpointLiveState>(mEndpointState.values());
		}
	}
	
	private static class EndpointLiveState
	{
		private final String mId;
		private final AtomicLong mTotalTime=new AtomicLong();
		private final AtomicLong mCount=new AtomicLong();
		private final AtomicLong mAvgCount=new AtomicLong();
		private long mLastAverage=0;
		
		public EndpointLiveState(String aId)
		{	mId=aId;
		}
		public String getId()
		{	return mId;
		}
		public void declareResponseTime(long aTime)
		{	mTotalTime.addAndGet(aTime);
			mCount.incrementAndGet();
			mAvgCount.incrementAndGet();
		}
		public long getAverageResponseTime()
		{
			long totalTime=mTotalTime.getAndSet(0L);
			long count=mAvgCount.getAndSet(0L);
			if (count>0)
			{	long lastAverage=totalTime/count;
				mLastAverage=lastAverage;
				return lastAverage;
			}
			else
			{	return mLastAverage;
			}
		}
		public long getExecutedCount()
		{	return mCount.get();
		}
	}
	
	private static class AgentLiveState
	{
		private final AtomicInteger mConcurrency=new AtomicInteger();
		private final String mInstanceId;
		private final AtomicLong mExecutedCount=new AtomicLong();
		private long mLastCount=-1;
		private long mLastTime=0;
		public AgentLiveState(String aInstanceId)
		{	mInstanceId=aInstanceId;
		}
		public int incrementConcurrency()
		{	return mConcurrency.incrementAndGet();
		}
		public int decrementConcurrency()
		{	return mConcurrency.decrementAndGet();
		}
		public boolean incrementIfLessThan(int aMax)
		{	while(true)
			{
				int value = mConcurrency.get();
			    if (value >= aMax) return false;
			    if (mConcurrency.compareAndSet(value, value+1)) return true;
			}
		}
		public int getConcurrency() { return mConcurrency.get(); }
		public String getInstanceId() { return mInstanceId; }
		public long getExecutedCount() { return mExecutedCount.get(); }
		public void incrementExecutedCount() { mExecutedCount.incrementAndGet(); }
		public float getExecutedRate(long aNow)
		{
			float result;
			long count=getExecutedCount();
			if (mLastTime>0)
			{	
				long tdiff=aNow-mLastTime;
				long cdiff=count-mLastCount;
				result=((float)cdiff*1000.0f)/((float)tdiff);
			}
			else
			{	result=0.0f;
			}
			mLastTime=aNow;
			mLastCount=count;
			return result;
		}
	};
	
	private AgentLiveState getOrCreateAgentState(String aAgentId)
	{
		AgentLiveState state=mAgentMap.get(aAgentId);
		if (state==null)
		{	AgentLiveState initialState=new AgentLiveState(aAgentId);
			AgentLiveState existing=mAgentMap.putIfAbsent(aAgentId,initialState);
			if(existing==null)
			{	state=initialState;
			}
			else
			{	state=existing;
			}
		}
		return state;
	}
	
	private NamespaceLiveState getOrCreateNamespaceState(String aName)
	{
		NamespaceLiveState state=mNamespaceMap.get(aName);
		if (state==null)
		{	NamespaceLiveState initialState=new NamespaceLiveState(aName);
			NamespaceLiveState existing=mNamespaceMap.putIfAbsent(aName,initialState);
			if(existing==null)
			{	state=initialState;
			}
			else
			{	state=existing;
			}
		}
		return state;
	}
	
	public int flush()
	{
		int count=0;
		for (NamespaceLiveState namespace : mNamespaceMap.values())
		{
			Deque<QueuedRequest> queue=namespace.getQueue();
			NKFException exception=new NKFException("Request Rejected","Sqwish rejected queued request because queue was flushed");
			QueuedRequest req;
			while ((req=queue.pollFirst())!=null)
			{
				INKFRequestContext reqContext=req.getContextForResponse();
				INKFResponse resp=reqContext.createResponseFrom(exception);
				resp.setExpiry(INKFResponse.EXPIRY_ALWAYS);
				IResponse rawResp=((NKFResponseImpl)resp).getKernelResponse();
				((NKFContextImpl)reqContext).handleAsyncResponse(rawResp);
				count++;
			}

			namespace.getAverageQueueWait();
		}
		return count;
	}
	
	public IHDSDocument getStatus()
	{
		long now=System.currentTimeMillis();
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("clientRequestRouter");
		
		m.pushNode("agents");
		List<AgentLiveState> states=new ArrayList<>(mAgentMap.values());
		for (AgentLiveState state : states)
		{
			m.pushNode("agent")
			.addNode("id", state.getInstanceId())
			.addNode("concurrency", state.getConcurrency())
			.addNode("executed", state.getExecutedCount())
			.addNode("executedRate", state.getExecutedRate(now))
			.popNode();
		}
		m.popNode();
		
		m.pushNode("namespaces");
		List<NamespaceLiveState> nstates=new ArrayList<>(mNamespaceMap.values());
		for (NamespaceLiveState state : nstates)
		{
			long accepted=state.getAcceptedRequestCount();
			long executed=state.getExecutedRequestCount();
			long completed=state.getCompletedRequestCount();
			long queueWait=state.getAverageQueueWait();
			m.pushNode("namespace")
			.addNode("id", state.getName())
			.addNode("queue", state.getQueue().size())
			.addNode("accepted", accepted)
			.addNode("executed", executed)
			.addNode("completed", completed)
			.addNode("queueWait", queueWait);
			
			m.pushNode("endpoints");
			for (EndpointLiveState epState : state.getEndpoints())
			{
				m.pushNode("endpoint")
				.addNode("id", epState.getId())
				.addNode("executed", epState.getExecutedCount())
				.addNode("responseTime", epState.getAverageResponseTime())
				.popNode();
			}
			m.popNode();
			
			
			m.popNode();
		}
		m.popNode();
		
		m.pushNode("clients");
		List<ClientLiveState> cstates=new ArrayList<>(mClientMap.values());
		for (ClientLiveState state : cstates)
		{
			m.pushNode("client")
			.addNode("id", state.getId())
			.addNode("namespace", state.getNamespace())
			.addNode("received", state.getReceivedRequestCount())
			.addNode("receivedRate", state.getReceivedRequestRate(now))
			.popNode();
		}
		m.popNode();
		
		m.declareKey("agentById", "/clientRequestRouter/agents/agent", "id");
		m.declareKey("namespaceById", "/clientRequestRouter/namespaces/namespace", "id");
		m.declareKey("clientById", "/clientRequestRouter/clients/client", "id");
		return m.toDocument(false);
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		if (!mIngress)
		{	throw new NKFException("Request Rejected","Sqwish rejected request because request ingress disabled");
		}
		else
		{
			String namespace=aContext.getThisRequest().getArgumentValue("namespace");
			String endpointId=aContext.getThisRequest().getArgumentValue("endpointId");
			ClientLiveState cls=ensureClientIsRegistered(aContext,namespace);
			cls.incrementReceivedRequests();
			Object requestBlob=aContext.source("arg:request");
			ensureConfig(aContext);
			NamespaceLiveState nls=getOrCreateNamespaceState(namespace);
			nls.incrementAcceptedCount();
			
			QueuedRequest qr=new QueuedRequest(namespace, endpointId, System.currentTimeMillis(), requestBlob, aContext);
			if (!routeRequest(qr, null, false, aContext))
			{	nls.getQueue().add(qr);
			}
			aContext.setNoResponse();
		}
	}
	
	ClientLiveState ensureClientIsRegistered(INKFRequestContext aContext, String aNamespace) throws Exception
	{
		
		String remoteInfo=(String)aContext.getThisRequest().getHeaderValue("NKP_REMOTE_INFO");
		if (remoteInfo!=null)
		{
			ClientLiveState result=mClientMap.get(remoteInfo);
			if (result==null)
			{
				//format of remoteInfo from Netty is /[IP]:[PORT]
				int i=remoteInfo.indexOf(':');
				String ip=remoteInfo.substring(1,i);
				String port=remoteInfo.substring(i+1);
				ClientLiveState c=new ClientLiveState(remoteInfo,ip,port,aNamespace);
				ClientLiveState existing=mClientMap.putIfAbsent(remoteInfo, c);
				if (existing==null)
				{	String msg=String.format("Client [%s] connected",remoteInfo);
					mContext.logRaw(INKFLocale.LEVEL_INFO, msg);
					result=c;
				}
				else
				{	result=existing;
				}
			}
			return result;
		}
		else
		{	return null;
		}
	}
	
	/* return true if queue successfully serviced */
	public boolean serviceQueue(INKFRequestContext aContext)
	{	
		if (mStopped || !mEgress) return false;
		
		boolean result=false;
		
		for (NamespaceLiveState nls : mNamespaceMap.values())
		{
			QueuedRequest qr= nls.getQueue().pollFirst();
			if (qr!=null)
			{	try
				{	if (!routeRequest(qr, null, true, qr.getContextForResponse()))
					{	nls.getQueue().push(qr);
					}
					else
					{	result=true;
					}
				}
				catch (Exception e)
				{
					aContext.logRaw(INKFLocale.LEVEL_WARNING, "Unhanded exception in ClientRequestRouter.serviceQueue:\n"+Utils.throwableToString(e));
				}
			}
		}
		//System.out.println("serviceQueue "+result);
		return result;
	}
	
	public boolean routeRequest(QueuedRequest aQR, String aAgentIdToAvoid, boolean aFrontOfQueue, INKFRequestContext aContext) throws Exception
	{	
		if (!mEgress) return false;
		
		IHDSReader routingTable=getRoutingTable(aContext);
		if (routingTable==null)
		{	return false;
		}
		IHDSReader agent=getAgentForRequest(aQR.getNamespace(), aAgentIdToAvoid, routingTable);
		if (agent==null)
		{	return false;
		}
		
		String agentId=(String)agent.getFirstValue("id");
		INKFRequestContext agentContext=(INKFRequestContext)agent.getFirstValue("context");
		
		INKFRequest req=agentContext.createRequest("active:receiveClientRequest");
		req.addArgument("namespace", aQR.getNamespace());
		req.addArgumentByValue("request", aQR.getRequestBlob());
		final INKFRequestContext clientRequestRouterContext=aContext;
		NamespaceLiveState nls=getOrCreateNamespaceState(aQR.getNamespace());
		AgentLiveState als=getOrCreateAgentState(agentId);
		nls.incrementExecCount();
		long startTime=System.currentTimeMillis();
		INKFAsyncRequestListener l=new INKFAsyncRequestListener()
		{
			@Override
			public void receiveResponse(INKFResponseReadOnly aResponse, INKFRequestContext aContext) throws Exception
			{
				long now=System.currentTimeMillis();
				long queueWait=startTime-aQR.getCreationTimestamp();
				long executionTime=now-startTime;
				nls.declareExecutionTime(aQR.getEndpointId(),executionTime);
				nls.declareQueueWait(queueWait);
				als.decrementConcurrency();
				als.incrementExecutedCount();
				INKFResponse resp=clientRequestRouterContext.createResponseFrom(aResponse);
				// hack - it seems sub-request doesn't get detected in dependencies
				IExpiry expiry=((NKFResponseReadOnlyImpl)aResponse).getMeta().getExpiry();
				clientRequestRouterContext.getKernelContext().declareDependency(expiry, 0);
				IResponse rawResp=((NKFResponseImpl)resp).getKernelResponse();
				((NKFContextImpl)clientRequestRouterContext).handleAsyncResponse(rawResp);
				
				aContext.setNoResponse();
				nls.incrementCompleteCount();
				serviceQueue(clientRequestRouterContext);
			}
			
			@Override
			public void receiveException(NKFException aException, INKFRequest aRequest, INKFRequestContext aContext) throws Exception
			{
				als.decrementConcurrency();
				Exception exceptionToReturn=aException;
				if (aException.getDeepestId().equals("Timeout"))
				{
					String msg=String.format("timeout on application request to [%s]", aQR.getNamespace());
					CMUtils.notifyAgentError(agentId,msg,clientRequestRouterContext);
					nls.decrementExecCount();
					//retry if possible
					try
					{	exceptionToReturn=null;
						if (!routeRequest(aQR,agentId,aFrontOfQueue,clientRequestRouterContext))
						{	
							if (aFrontOfQueue)
							{	nls.getQueue().push(aQR);
							}
							else
							{	nls.getQueue().add(aQR);
							}
						}
					}
					catch (Exception e)
					{
						exceptionToReturn=e;
					}
				}
				if (exceptionToReturn!=null)
				{	
					long now=System.currentTimeMillis();
					long queueWait=startTime-aQR.getCreationTimestamp();
					long executionTime=now-startTime;
					nls.declareExecutionTime(aQR.getEndpointId(),executionTime);
					nls.declareQueueWait(queueWait);
					als.incrementExecutedCount();
					
					//send exception back to client
					aContext.setNoResponse();
					INKFResponse resp=clientRequestRouterContext.createResponseFrom(aException);
					resp.setExpiry(INKFResponse.EXPIRY_ALWAYS);
					IResponse rawResp=((NKFResponseImpl)resp).getKernelResponse();
					((NKFContextImpl)clientRequestRouterContext).handleAsyncResponse(rawResp);
					
					nls.incrementCompleteCount();
					serviceQueue(clientRequestRouterContext);
				}
				
				aContext.setNoResponse();
				
			}
		};
		agentContext.issueAsyncRequest(req).setListenerTimeout(mAgentTimeout, mTimer).setListener(l);
		aContext.setNoResponse();
		return true;
	}
	
	final int RAMP_UP_TIME=8000;
	
	private IHDSReader getAgentForRequest(String aNamespace, String aAgentIdToAvoid, IHDSReader aRoutingTable)
	{
		long now=System.currentTimeMillis();
		//IHDSReader routingTableForNamespace=aRoutingTable.getFirstNodeOrNull("/namespaces/namespace[id='"+aNamespace+"']");
		IHDSReader routingTableForNamespace=aRoutingTable.getFirstNodeOrNull("key('byId','"+aNamespace+"')");
		if (routingTableForNamespace!=null)
		{
			List <IHDSReader> agents=routingTableForNamespace.getNodes("agent");
			int size=agents.size();
			
			if (size>0)
			{	
				IHDSReader result=null;
				int randomIndex=(int)(Math.random()*size);
				for (int i=0; i<size; i++)
				{
					IHDSReader agent=agents.get(randomIndex);
					String agentId=(String)agent.getFirstValue("id");
					if (aAgentIdToAvoid==null || !agentId.equals(aAgentIdToAvoid))
					{
						AgentLiveState als=getOrCreateAgentState(agentId);
						Integer cores=Integer.parseInt((String)agent.getFirstValue("cores"));
						Long connectTime=(Long)agent.getFirstValue("lastConnect");
						long age=now-connectTime;
						int concurrency=cores+2;
						if (age<RAMP_UP_TIME)
						{
							concurrency=(int)(age*(cores+2)/RAMP_UP_TIME);
							//if (concurrency<1) concurrency=1;
						}
						//System.out.println("concurrency="+concurrency);
						
						if (als.incrementIfLessThan(concurrency))
						{	result=agent;
							break;
						}
					}
				}
				return result;
			}
		}
		return null;
	}
	
}
