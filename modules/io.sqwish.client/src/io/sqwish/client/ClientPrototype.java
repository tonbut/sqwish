package io.sqwish.client;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.netkernel.container.IKernel;
import org.netkernel.container.IMessages;
import org.netkernel.container.config.IConfiguration;
import org.netkernel.layer0.bnf.BNFGrammarFactory;
import org.netkernel.layer0.boot.BootUtils;
import org.netkernel.layer0.meta.IEndpointMeta;
import org.netkernel.layer0.meta.IIdentifierGrammar;
import org.netkernel.layer0.meta.ISpaceElements;
import org.netkernel.layer0.meta.impl.EndpointMetaBuilder;
import org.netkernel.layer0.meta.impl.MetadataUtils;
import org.netkernel.layer0.meta.impl.SpaceElementsImpl;
import org.netkernel.layer0.nkf.INKFAsyncRequestListener;
import org.netkernel.layer0.nkf.INKFLocale;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFRequestReadOnly;
import org.netkernel.layer0.nkf.INKFResolutionContext;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.layer0.representation.ByteArrayRepresentation;
import org.netkernel.layer0.representation.IHDSNode;
import org.netkernel.layer0.representation.impl.HDSBuilder;
import org.netkernel.layer0.urii.SimpleIdentifierImpl;
import org.netkernel.layer0.util.TimedExpiryFunction;
import org.netkernel.layer0.util.TupleList;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.RootSpace;
import org.netkernel.module.standard.SpaceFactory;
import org.netkernel.module.standard.endpoint.ConfiguredEndpointImpl;
import org.netkernel.request.IRequest;
import org.netkernel.request.IRequestResponseFields;
import org.netkernel.request.impl.RequestResponseFieldsImpl;
import org.netkernel.urii.IEndpoint;
import org.netkernel.urii.IMetaRepresentation;
import org.netkernel.urii.ISpace;
import org.netkernel.util.Utils;
import org.netkernel.xml.util.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class ClientPrototype extends ConfiguredEndpointImpl
{ 
	private String mNamespace;
	private RootSpace mInnerSpace;
	private String mStatus;
	
	
	public ClientPrototype()
	{	this.declareThreadSafe();
	}
	
	@Override
	protected void postCommission(INKFRequestContext aContext) throws Exception
	{
		super.postCommission(aContext);	
		initialiseInnerSpace(aContext);
	}
	
	@Override
	protected void preDecommission(INKFRequestContext aContext) throws Exception
	{
		if (mInnerSpace!=null)
		{	
			try
			{	mInnerSpace.preDecommissionSpace();
			}
			catch (Exception e) { ; }
			try
			{	mInnerSpace.onDecommissionSpace();
			}
			catch (Exception e) { ; }
		}
	}
	
	private void initialiseInnerSpace(INKFRequestContext aContext) throws Exception
	{
		if (mNamespace==null)
		{
			mNamespace=(String)aContext.getParamValue("namespace");
			
			//create definition of inner dynamic space
			IHDSReader config=aContext.source(getConfigResourceIdentifier(aContext),IHDSDocument.class).getReader().getFirstNode("/*");
			String host=(String)config.getFirstValue("host");
			String port=(String)config.getFirstValue("port");
			String username=(String)config.getFirstValue("username");
			String password=(String)config.getFirstValue("password");
			String tls=(String)config.getFirstValue("tls");
			ISpace clientSpace=aContext.getKernelContext().getKernel().getSpace(new SimpleIdentifierImpl("urn:io:sqwish:client"), null, null);
			INKFRequest req=aContext.createRequest("res:/io/sqwish/client/SpaceTemplate.xml");
			req.injectRequestScope(clientSpace);
			req.setRepresentationClass(IHDSDocument.class);
			IHDSMutator spaceTemplate=((IHDSDocument)aContext.issueRequest(req)).getMutableClone();
			spaceTemplate.setCursor("/module/rootspace/endpoint/config/tunnel/host").setValue(host);
			spaceTemplate.setCursor("/module/rootspace/endpoint/config/tunnel/port").setValue(port);
			spaceTemplate.setCursor("/module/rootspace/endpoint/config/username").setValue(username);
			spaceTemplate.setCursor("/module/rootspace/endpoint/config/password").setValue(password);
			
			spaceTemplate.setCursor("/module/rootspace/endpoint/config/tunnel");
			if (tls.equals("true"))
			{	spaceTemplate.addNode("tls", "true");
			}
			else if (tls.equals("explicitTrust"))
			{	spaceTemplate.addNode("tls", "true");
				spaceTemplate.addNode("tlsExplicitTrust", "true");
			}
			
			//instantiate inner space
			Document spaceDOM=aContext.transrept(spaceTemplate.toDocument(false), Document.class);
			Element spaceElement=XMLUtils.getFirstChildElement(spaceDOM.getDocumentElement());
			RootSpace space=new RootSpace(getStandardSpace().getOwningModule(),false,spaceElement);
			IKernel k=aContext.getKernelContext().getKernel();
			SpaceFactory.innerBuildSpace(space, spaceElement, k);
			space.onCommissionSpace(k);
			space.postCommissionSpace();
			mInnerSpace=space;
			
			
			// issue META request to start space initialising
			req=aContext.createRequest("");
			req.setVerb(INKFRequestReadOnly.VERB_META);
			req.injectRequestScope(mInnerSpace);
			req.setHeader(IRequest.HEADER_EXCLUDE_DEPENDENCIES, true);
			aContext.issueRequest(req);
		}
	}
	
	private static String getConfigResourceIdentifier(INKFRequestContext aContext) throws Exception
	{
		IConfiguration config = aContext.getKernelContext().getKernel().getConfiguration();
		URI base = URI.create(BootUtils.getInstallPath(config));
		URI configURI=base.resolve(URI.create("etc/sqwishClientConfig.xml")); 
		return configURI.toString();
	}
	
	@Override
	protected IConfig createConfig(INKFRequestContext aContext) throws Exception
	{	initialiseInnerSpace(aContext);
		
		return new ClientConfig(aContext,this,mInnerSpace,mNamespace);
	}
	
	@Override
	protected void destroyConfig(IConfig aConfig, boolean aFinal) throws Exception
	{	
		if (aConfig instanceof ClientConfig)
		{
			((ClientConfig)aConfig).destroy();
		}
	}
	
	public IRequestResponseFields getState()
	{	RequestResponseFieldsImpl fields=new RequestResponseFieldsImpl(super.getState());
		
		fields.put("status", mStatus);
		return fields;
	}
	
	private void setStatus(String aStatus)
	{	mStatus=aStatus;
	}
	
	
	
	private static class ClientConfig implements IConfig, INKFAsyncRequestListener
	{
		private Map<String, IEndpointMeta> mMetaMap=Collections.EMPTY_MAP;
		private ISpaceElements mMetaElements=SpaceElementsImpl.EMPTY;
		
		private final String mNamespace;
		private final ISpace mSpace;
		private Method mMarshalMethod;
		
		public ClientConfig(INKFRequestContext aContext, ClientPrototype aEndpoint, ISpace aSpace, String aNamespace) throws Exception
		{
			mNamespace=aNamespace;
			mSpace=aSpace;
			
			StringBuilder sb=new StringBuilder();
			
			sb.append("start. ");
			boolean success=false;
			try
			{
				Class nkpUtilsLight=this.getClass().getClassLoader().loadClass("com.ten60.netkernel.nkp.roc.common.NKPLightUtils");
				mMarshalMethod=nkpUtilsLight.getMethod("marshalRequest", INKFRequestReadOnly.class, INKFRequestContext.class);
			
				sb.append("got nkputils. ");
				if (mSpace!=null)
				{
					INKFRequest req2=aContext.createRequest("active:nkpClientStatus");
					req2.injectRequestScope(mSpace);
					req2.setHeader(IRequest.HEADER_EXCLUDE_DEPENDENCIES, true);
					req2.setRepresentationClass(IHDSNode.class);
					IHDSNode status=(IHDSNode)aContext.issueRequest(req2);
					String state=status.getFirstValue("/state").toString();
					String remoteMeta=(String)status.getFirstValue("/remoteMetadata");
					sb.append("nkpState="+state+". ");
					sb.append("remoteMeta="+remoteMeta+". ");
					//System.out.println("SqwishClient.syncronizeRemoteMetadata "+state+" "+remoteMeta);
					//if (state.equals("UP") && remoteMeta.equals("Synchronized"))
					{
						aContext.logRaw(INKFLocale.LEVEL_INFO, "Client requesting namespace metadata update");
						req2=aContext.createRequest("active:namespaceMetadata");
						req2.addArgument("namespace", mNamespace);
						req2.injectRequestScope(mSpace);
						//req2.setHeader(IRequest.HEADER_EXCLUDE_DEPENDENCIES, true);
						req2.setRepresentationClass(IHDSDocument.class);
						INKFResponseReadOnly<IHDSDocument> resp=aContext.issueRequestForResponse(req2);
						IHDSDocument metadata=resp.getRepresentation();
						if (metadata!=null)
						{
							updateRemoteMetadata(metadata, aContext, aEndpoint);
							success=true;
							sb.append("endpoints="+mMetaElements.size()+" success=true. ");
							aContext.logRaw(INKFLocale.LEVEL_INFO, "Client received "+mMetaElements.size()+" endpoints");
						}
					}
				}
			}
			catch (NKFException e)
			{	
				String id=e.getDeepestId();
				if (!id.equals("Request Resolution Failure"))
				{
					String msg="error in syncronizedRemoteMetadata:\n"+Utils.throwableToString(e);
					aContext.logRaw(INKFLocale.LEVEL_WARNING, msg);
				}
				sb.append("exception="+id+" "+e.getDeepestMessage()+". ");
			}
			
			if (!success)
			{
				aContext.getKernelContext().declareDependency(new TimedExpiryFunction("", System.currentTimeMillis()+1000L), 0);
			}
			aEndpoint.setStatus(sb.toString());
			
		}
		
		private void updateRemoteMetadata(IHDSDocument aMetadata, INKFRequestContext aContext, IEndpoint aEndpoint) throws Exception
		{
			List<IHDSReader> endpoints=aMetadata.getReader().getNodes("/endpoints/endpoint");
			Map<String,IEndpointMeta> logicalEndpoints=new LinkedHashMap<String, IEndpointMeta>();
			TupleList parts = new TupleList(3,endpoints.size());
			for (IHDSReader endpoint : endpoints)
			{
				String id=(String)endpoint.getFirstValue("id");
				parts.put(new SimpleIdentifierImpl(id), null ,aEndpoint);
				EndpointMetaBuilder epmb=new EndpointMetaBuilder();
				epmb.setName((String)endpoint.getFirstValue("name"));
				epmb.setDescription((String)endpoint.getFirstValue("desc"));
				epmb.setVerbs(Integer.parseInt((String)endpoint.getFirstValue("verbs")));
				IMessages messages=aContext.getKernelContext().getKernel().getLogger();
				String grammarString=(String)endpoint.getFirstValue("grammar");
				Element grammarElement=XMLUtils.parse(new StringReader(grammarString)).getDocumentElement();
				IIdentifierGrammar grammar=BNFGrammarFactory.parse(grammarElement, messages);
				epmb.setGrammar(grammar);
				IEndpointMeta epMeta=epmb.getMeta(messages);
				logicalEndpoints.put(id,epMeta);
			}
			//System.out.println("installing new metadata in client");
			mMetaElements=new SpaceElementsImpl(parts);
			mMetaMap=logicalEndpoints;
		}
		
		public void destroy()
		{
		}
		
		@Override
		public ISpaceElements getElements(INKFRequestContext aContext) throws Exception
		{
			return mMetaElements;
		}

		@Override
		public IMetaRepresentation getElementMeta(String aElementId)
		{
			return mMetaMap.get(aElementId);
		}

		@Override
		public String getResolvedElement(INKFResolutionContext aContext) throws Exception
		{
			String result=null;
			INKFRequestReadOnly toResolve=aContext.getRequestToResolve();
			for (Map.Entry<String, IEndpointMeta> entry : mMetaMap.entrySet())
			{	IEndpointMeta meta = entry.getValue();
				if (MetadataUtils.match((IEndpointMeta)meta, toResolve, aContext))
				{	result = entry.getKey();
					break;
				}
			}
			return result;
		}

		@Override
		public void onRequest(String aEndpointId, INKFRequestContext aContext) throws Exception
		{
			ByteArrayRepresentation serialized=marshalRequest(aContext.getThisRequest(), aContext);
			INKFRequest req=aContext.createRequest("active:routeClientRequest");
			req.addArgument("namespace", mNamespace);
			req.addArgument("endpointId", aEndpointId);
			req.addArgumentByValue("request", serialized);
			req.injectRequestScope(mSpace);
			aContext.issueAsyncRequest(req).setListener(this);
			aContext.setNoResponse();
		}
		
		private ByteArrayRepresentation marshalRequest(INKFRequestReadOnly aRequest, INKFRequestContext aContext) throws Exception
		{
			ByteArrayOutputStream baos=(ByteArrayOutputStream)mMarshalMethod.invoke(null, aRequest,aContext);
			return new ByteArrayRepresentation(baos);
		}

		@Override
		public ISpace[] getSpaces(INKFRequestContext aContext) throws Exception
		{	return mSpace!=null?new ISpace[] { mSpace } : ConfiguredEndpointImpl.NO_SPACES;
		}

		@Override
		public void receiveResponse(INKFResponseReadOnly aResponse, INKFRequestContext aContext) throws Exception
		{
			aContext.createResponseFrom(aResponse);
		}

		@Override
		public void receiveException(NKFException aException, INKFRequest aRequest, INKFRequestContext aContext)
				throws Exception
		{
			throw aException;
		}
	}
		
}
