package io.sqwish.agent;

import java.net.URI;

import org.netkernel.container.config.IConfiguration;
import org.netkernel.layer0.boot.BootUtils;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class NKPConfigAccessor extends StandardAccessorImpl
{
	public NKPConfigAccessor()
	{
		this.declareThreadSafe();
	}
	
	private static String getConfigResourceIdentifier(INKFRequestContext aContext) throws Exception
	{
		IConfiguration config = aContext.getKernelContext().getKernel().getConfiguration();
		URI base = URI.create(BootUtils.getInstallPath(config));
		URI configURI=base.resolve(URI.create("etc/sqwishAgentConfig.xml")); 
		return configURI.toString();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		IHDSReader config=aContext.source(getConfigResourceIdentifier(aContext),IHDSDocument.class).getReader().getFirstNode("/*");
		String host=(String)config.getFirstValue("host");
		String port=(String)config.getFirstValue("port");
		String username=(String)config.getFirstValue("username");
		String password=(String)config.getFirstValue("password");
		String tls=(String)config.getFirstValue("tls");
		
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("config")
		.addNode("passByValue", "true")
		.addNode("exposeRequestScope", "true")
		.addNode("statusResourceIdentifier", "active:NKPConnectionStatus")
		.addNode("username", username)
		.addNode("password", password)
		.pushNode("tunnel")
		.addNode("@factory", "com.ten60.netkernel.nkp.netty.NettyNKPTunnelFactory")
		.addNode("host", host)
		.addNode("port", port);
		
		if (tls.equals("true"))
		{	m.addNode("tls", "true");
		}
		else if (tls.equals("explicitTrust"))
		{	m.addNode("tls", "true");
			m.addNode("tlsExplicitTrust", "true");
		}

		m.popNode()
		.popNode();
		aContext.createResponseFrom(m.toDocument(false));
	}
}
