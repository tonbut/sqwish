package io.sqwish.agent;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.netkernel.layer0.boot.IModule;
import org.netkernel.layer0.boot.ModuleManager;
import org.netkernel.layer0.boot.ModuleSourceWithLevel;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.representation.IHDSNode;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class DeployedModulesListAccessor extends StandardAccessorImpl
{
	public DeployedModulesListAccessor()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		ServerConnectionTransport.getInstance().ping();
		Set<String> existingModules = SynchronizeDeployedModulesAccessor.getDeployedModulesSources(aContext);
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("modules");
		
		
		Map<URI,ModuleSourceWithLevel> registeredModules=ModuleManager.getSingleton().getRegisteredModules();
		List<IModule> modules=ModuleManager.getSingleton().getModules();
		
		for (URI source : registeredModules.keySet())
		{
			if (existingModules.contains(source.toString()))
			{
				IModule found=null;
				for (IModule module : modules)
				{	if (module.getSource().equals(source))
					{	found=module;
						break;
					}
				}
				if (found!=null)
				{
					String uri=found.getMeta().getIdentifier();
					String version=found.getMeta().getVersion();
					
					//MD5
					INKFRequest req = aContext.createRequest("active:md5");
					req.addArgument("operand", source.toString());
					req.setRepresentationClass(String.class);
					String md5=(String)aContext.issueRequest(req);
					
					m.pushNode("module")
					.addNode("id", uri)
					.addNode("version", version)
					.addNode("content_md5", md5)
					.popNode();
				}
			}
		}
		
		
		/*
		IHDSNode list=aContext.source("active:moduleListDoc", IHDSNode.class);
		for (IHDSNode module: list.getNodes("/modules/module"))
		{	String source=(String)module.getFirstValue("source");
			if (existingModules.contains(source))
			{	String uri=(String)module.getFirstValue("id");
				String version=(String)module.getFirstValue("version");
				
				//MD5
				INKFRequest req = aContext.createRequest("active:md5");
				req.addArgument("operand", source);
				req.setRepresentationClass(String.class);
				String md5=(String)aContext.issueRequest(req);
				
				m.pushNode("module")
				.addNode("id", uri)
				.addNode("version", version)
				.addNode("content_md5", md5)
				.popNode();
			}
		}
		*/
		
		INKFResponse resp=aContext.createResponseFrom(m.toDocument(false));
	}
}
