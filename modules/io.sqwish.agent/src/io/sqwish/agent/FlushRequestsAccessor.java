package io.sqwish.agent;

import java.util.ArrayList;
import java.util.List;

import org.netkernel.layer0.nkf.INKFLocale;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.util.Utils;

public class FlushRequestsAccessor extends StandardAccessorImpl
{
	public FlushRequestsAccessor()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		aContext.logRaw(INKFLocale.LEVEL_INFO, "Flushing all pending requests");
		
		//get list of root requests 
		IHDSReader threads=aContext.source("netkernel:/k",IHDSDocument.class).getReader();
		List<Thread> threadList=new ArrayList<Thread>();
		List<IHDSReader> roots=threads.getNodes("/k/roots/state");
		for (IHDSReader root : roots)
		{
			String request=(String)root.getFirstValue("request");
			if (request.startsWith("SOURCE active:receiveClientRequest"))
			{
				String rootId=root.getFirstValue("root-id").toString();
				try
				{
					INKFRequest req=aContext.createRequest("active:kill");
					req.addArgument("root", rootId);
					req.addArgumentByValue("reason", "Sqwish Flush");
					Thread t=new Thread() {
						public void run()
						{
							try
							{	aContext.issueRequest(req);
							}
							catch (Exception e)
							{
								aContext.logRaw(INKFLocale.LEVEL_INFO,Utils.throwableToString(e));
							}
						}
					};
					t.start();
					threadList.add(t);
				}
				catch (NKFException e)
				{
					aContext.logRaw(INKFLocale.LEVEL_INFO,Utils.throwableToString(e));
				}
			}
		}
		for (Thread t : threadList)
		{	t.join();
		}
		
		
		aContext.createResponseFrom(Integer.toString(threadList.size())).setExpiry(INKFResponse.EXPIRY_ALWAYS);
	}
}
