package io.sqwish.agent;

import java.io.File;
import java.net.URI;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.netkernel.container.config.IConfiguration;
import org.netkernel.layer0.boot.BootUtils;
import org.netkernel.layer0.nkf.INKFLocale;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.util.Utils;
import org.netkernel.xml.util.XMLUtils;
import org.netkernel.xml.xda.DOMXDA;
import org.netkernel.xml.xda.IXDAReadOnly;
import org.netkernel.xml.xda.IXDAReadOnlyIterator;

public class SynchronizeDeployedModulesAccessor extends StandardAccessorImpl
{
	public static final String MODULESD_FILENAME="sqwish_modules.xml";
	
	public SynchronizeDeployedModulesAccessor()
	{	this.declareThreadSafe();
	}
	
	public static String getModulesXMLFile(INKFRequestContext aContext) throws Exception
	{
		IConfiguration config = aContext.getKernelContext().getKernel().getConfiguration();
		URI base = URI.create(BootUtils.getInstallPath(config));
		URI modulesdir = BootUtils.getKernelURIProperty(config, "netkernel.init.modulesdir");
		if (modulesdir == null)
		{	modulesdir = URI.create("etc/modules.d/"); 
		}
		if (!modulesdir.isAbsolute())
		{	modulesdir = base.resolve(modulesdir);
		}
		if (!modulesdir.getScheme().equals("file"))
		{	throw new NKFException("netkernel.init.modulesdir must be a filesystem directory");
		}
		return modulesdir.resolve(MODULESD_FILENAME).toString();
	}
	
	public static Set<String> getDeployedModulesSources(INKFRequestContext aContext) throws Exception
	{
		Set<String> existingModules;
		String modulesFileURI=getModulesXMLFile(aContext);
		if (aContext.exists(modulesFileURI))
		{
			//get list of modules deployed currently
			existingModules = new HashSet<>();
			IXDAReadOnly existingFile=aContext.source(modulesFileURI,IXDAReadOnly.class);
			IXDAReadOnlyIterator i=existingFile.readOnlyIterator("/modules/module");
			while(i.hasNext())
			{	i.next();
				String src=i.getText(".", true);
				existingModules.add(src);
			}
		}
		else
		{	existingModules=Collections.EMPTY_SET;
		}
		return existingModules;
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		
		IHDSReader operand=aContext.source("arg:operand",IHDSDocument.class).getReader();
		Boolean force=aContext.source("arg:force",Boolean.class);
		
		String modulesFileURI=getModulesXMLFile(aContext);
		
		//check if deployment has changed
		Set<String> existingModules = getDeployedModulesSources(aContext);
		
		boolean changed=false;
		for (IHDSReader module : operand.getNodes("/modules/module"))
		{	String src=(String)module.getFirstValue("src");
			if (!existingModules.remove(src))
			{	changed=true;
				break;
			}
		}
		if (existingModules.size()>0)
		{	//some have been removed
			changed=true;
		}
		
		//update if changed
		boolean reload=changed || force;
		if (reload)
		{
			
			DOMXDA xda=new DOMXDA(XMLUtils.newDocument());
			xda.appendPath("/", "modules", null);
			for (IHDSReader module : operand.getNodes("/modules/module"))
			{
				String src=(String)module.getFirstValue("src");
				xda.appendPath("/modules", "module", src);
			}
			
			String newFileContents=XMLUtils.toXML(xda.getRoot(), true, false);
			aContext.sink(modulesFileURI, newFileContents);
		}
		
		INKFResponse resp=aContext.createResponseFrom(reload);
		resp.setExpiry(INKFResponse.EXPIRY_ALWAYS);
		
	}	
}
