package io.sqwish.agent;

import java.io.InputStream;
import java.lang.reflect.Method;

import org.netkernel.layer0.nkf.INKFAsyncRequestListener;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.layer0.representation.IReadableBinaryStreamRepresentation;
import org.netkernel.layer0.urii.SimpleIdentifierImpl;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.urii.ISpaceWithIdentity;

public class ReceiveClientRequestAccessor extends StandardAccessorImpl implements INKFAsyncRequestListener
{
	private Method mUnmarshalMethod;
	
	public ReceiveClientRequestAccessor()
	{	this.declareThreadSafe();
	}
	
	@Override
	protected void postCommission(INKFRequestContext aContext) throws Exception
	{
		super.postCommission(aContext);
		
		Class nkpUtilsLight=this.getClass().getClassLoader().loadClass("com.ten60.netkernel.nkp.roc.common.NKPLightUtils");
		mUnmarshalMethod=nkpUtilsLight.getMethod("unmarshalRequest", InputStream.class, INKFRequestContext.class);
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String namespace=aContext.getThisRequest().getArgumentValue("namespace");
		IReadableBinaryStreamRepresentation requestBlob=aContext.source("arg:request",IReadableBinaryStreamRepresentation.class);
		INKFRequest req=(INKFRequest)mUnmarshalMethod.invoke(null, requestBlob.getInputStream(), aContext);
		ISpaceWithIdentity space=this.getStandardSpace().getOwningModule().getSpace(new SimpleIdentifierImpl("urn:io:sqwish:agent:dynamic"));
		req.injectRequestScope(space);
		//aContext.createResponseFrom(aContext.issueRequestForResponse(req));
		aContext.issueAsyncRequest(req).setListener(this);
		aContext.setNoResponse();
	}

	@Override
	public void receiveResponse(INKFResponseReadOnly aResponse, INKFRequestContext aContext) throws Exception
	{	aContext.createResponseFrom(aResponse);		
	}

	@Override
	public void receiveException(NKFException aException, INKFRequest aRequest, INKFRequestContext aContext) throws Exception
	{	throw aException;
	}
}
