package io.sqwish.agent;

import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.representation.IHDSNode;
import org.netkernel.layer0.representation.IHDSNodeList;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;

public class InstanceInfoAccessor extends StandardAccessorImpl
{
	public InstanceInfoAccessor()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		ServerConnectionTransport.getInstance().ping();
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("info");
		
		String processors=Integer.toString(Runtime.getRuntime().availableProcessors());
		String corePerformance=aContext.getKernelContext().getKernel().getConfiguration().getString("netkernel.corePerformance", "");
		IHDSNode memory=aContext.source("netkernel:/memory",IHDSNode.class);
		IHDSNodeList stats=memory.getNodes("/memory/stat");
		if (stats.size()>0)
		{
			IHDSNode lastStat=stats.get(stats.size()-1);
			int cpu=((Number)lastStat.getFirstValue("cpu")).intValue();
			if (cpu>100) cpu=100;
			m.pushNode("heap")
			.addNode("baseline",lastStat.getFirstValue("baseline"))
			.addNode("peak",lastStat.getFirstValue("peak"))
			.addNode("alloc",lastStat.getFirstValue("alloc"))
			.popNode()
			.pushNode("cache")
			.addNode("size",lastStat.getFirstValue("cacheSize"))
			.addNode("peakSize",lastStat.getFirstValue("cacheSizePeak"))
			.addNode("troughSize",lastStat.getFirstValue("cacheSizeTrough"))
			.addNode("hitRate",lastStat.getFirstValue("cacheHitRate"))
			.popNode()
			.pushNode("requests")
			.addNode("requestRate",lastStat.getFirstValue("requests"))
			.addNode("activeRootRequests",lastStat.getFirstValue("activeRootRequests"))
			.popNode()
			.pushNode("threads")
			.addNode("total",lastStat.getFirstValue("threadTotal"))
			.addNode("poolSize",lastStat.getFirstValue("threadPoolSize"))
			.addNode("poolBusy",lastStat.getFirstValue("threadPoolBusy"))
			.popNode()
			.pushNode("cpu")
			.addNode("userCPU",cpu)
			.addNode("gcCPU",lastStat.getFirstValue("gctime"))
			.addNode("processors", processors)
			.addNode("corePerformance", corePerformance)
			.popNode();
			
		}
		
		INKFResponse resp=aContext.createResponseFrom(m.toDocument(false));
		resp.setExpiry(INKFResponse.EXPIRY_ALWAYS);
	}
	
}
