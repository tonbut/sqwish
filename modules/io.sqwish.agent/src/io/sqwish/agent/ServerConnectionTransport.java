package io.sqwish.agent;

import java.util.concurrent.atomic.AtomicInteger;

import org.netkernel.container.ISpaceListener;
import org.netkernel.layer0.nkf.INKFAsyncRequestListener;
import org.netkernel.layer0.nkf.INKFLocale;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.layer0.nkf.NKFException;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardTransportImpl;

public class ServerConnectionTransport extends StandardTransportImpl implements ISpaceListener
{
	enum ConnectionState
	{
		START,
		READY_TO_CONNECT,
		CONNECTING,
		CONNECTED,
		DISABLED,
		SHUTDOWN,
		DECOMMISSION,
		WAIT_FOR_REBOOT
	}
	private ConnectionState mState = ConnectionState.START;
	
	private ServerConnectionThread mConnectionThread;
	private static ServerConnectionTransport sInstance;
	private long mLastPing;
	
	
	public ServerConnectionTransport()
	{	declareThreadSafe();
		sInstance=this;
	}
	
	public static ServerConnectionTransport getInstance()
	{	return sInstance;
	}
	
	public void ping()
	{	mLastPing=System.currentTimeMillis();
	}
	
	public ConnectionState getConnectionState()
	{	return mState;
	}
	
	public void serverCallbackReceived()
	{	if (mState == ConnectionState.CONNECTING)
		{	getTransportContext().logRaw(INKFLocale.LEVEL_INFO,"Connected to Cluster Manager!");
			mState=ConnectionState.CONNECTED;
		}
	}
	
	public void disconnectAndWaitForReboot()
	{
		if (mState==ConnectionState.CONNECTED)
		{
			mState=ConnectionState.WAIT_FOR_REBOOT;
			try
			{	mConnectionThread.disconnect();
			}
			catch (Exception e)
			{ }
			
		}
	}

	@Override
	protected void postCommission(INKFRequestContext aContext) throws Exception
	{	mConnectionThread = new ServerConnectionThread(getTransportContext());
		mConnectionThread.start();
		aContext.getKernelContext().getKernel().addSpaceListener(this);
		
	}

	@Override
	protected void preDecommission(INKFRequestContext aContext) throws Exception
	{	aContext.getKernelContext().getKernel().removeSpaceListener(this);
		boolean disconnect=mState==ConnectionState.CONNECTED;
		mConnectionThread.clientShutdown();
		mConnectionThread.interrupt();
		if (disconnect) mConnectionThread.disconnect();
		
		
	}
	
	private class ServerConnectionThread extends Thread implements INKFAsyncRequestListener
	{
		private final INKFRequestContext mTransportContext;
		private AtomicInteger mActiveConnections=new AtomicInteger();
		
		public ServerConnectionThread(INKFRequestContext aContext)
		{	mTransportContext=aContext;
		}
		
		@Override
		public void run()
		{
			boolean isCommissioned = true;
	    	boolean enabled=true;
	    	INKFResponseReadOnly<IHDSDocument> configResponse=null;
	    	ConnectionState lastState=ConnectionState.SHUTDOWN;
	    	long lastConnectAttempt=System.currentTimeMillis();

			while (isCommissioned)
			{
				if (!mState.equals(lastState))
				{	mContext.logRaw(INKFLocale.LEVEL_INFO, "ServerConnection state="+mState);
					lastState=mState;
				}
				try
				{
					// we don't want to sit in a tight loop for possibly long-lived states
					if (mState == ConnectionState.DISABLED || mState == ConnectionState.READY_TO_CONNECT || mState == ConnectionState.CONNECTING || mState == ConnectionState.CONNECTED || mState == ConnectionState.WAIT_FOR_REBOOT)
					{	Thread.sleep(1000);
					}
					
					long now=System.currentTimeMillis();

					switch (mState) {
						case START:
							if (enabled)
							{	mState = ConnectionState.READY_TO_CONNECT;
							}
							else
							{	mState = ConnectionState.DISABLED;
							}
							break;
						case DISABLED:
							if (enabled)
							{	mState = ConnectionState.READY_TO_CONNECT;
							}
							break;
						case CONNECTING:
							if ((now-lastConnectAttempt)>5000)
							{	mState = ConnectionState.READY_TO_CONNECT;
							}
						case READY_TO_CONNECT:
							lastConnectAttempt=now;
							connect();
							break;
						case CONNECTED:
							if (!enabled)
							{	mState = ConnectionState.DISABLED;
								disconnect();
							}
							else
							{	verifyConnection();
							}
							break;
						case SHUTDOWN:
							mState = ConnectionState.DECOMMISSION;
							//disconnect();
							break;
						case DECOMMISSION:
							isCommissioned = false;
							break;
					}

				}
				catch (Exception e)
				{ //keep quiet
				}
			}
		}
		
		private void verifyConnection() throws Exception
		{
			boolean reconnect=false;
			long now=System.currentTimeMillis();
			if (now-mLastPing>20000L)
			{
				reconnect=true;
			}
			else
			{	IHDSReader status=mContext.source("active:NKPConnectionStatus",IHDSDocument.class).getReader();
				String state=status.getFirstValue("/state").toString();
				if (!state.equals("UP"))
				{	reconnect=true;
				}
			}
			
			if (reconnect)
			{	mContext.logRaw(INKFLocale.LEVEL_WARNING, "Unexpected loss of connection to Cluster Manager");
				mState = ConnectionState.READY_TO_CONNECT;
			}
		}

		private void connect() throws NKFException
		{
			if (mState != ConnectionState.READY_TO_CONNECT)
			{
				return;
			}

			ping();
			String instanceId = mTransportContext.source("netkernel:/config/netkernel.instance.identifier", String.class);

			mActiveConnections.incrementAndGet();
			INKFRequest req = mTransportContext.createRequest("active:agentConnect");
			req.addArgumentByValue("instanceId", instanceId);
			mState = ConnectionState.CONNECTING;
			mTransportContext.issueAsyncRequest(req).setListener(this);
			
			
		}
		
		private void disconnect() throws NKFException
		{
			String instanceId = mTransportContext.source("netkernel:/config/netkernel.instance.identifier", String.class);
			//System.out.println("issuing remote disconnect");
			INKFRequest req = mTransportContext.createRequest("active:agentDisconnect");
			req.addArgumentByValue("instanceId", instanceId);
			mTransportContext.issueAsyncRequest(req);
			try
			{	Thread.sleep(100);
			} catch(Exception e) {;}
		}

		public void clientShutdown()
		{
			mState=ConnectionState.DECOMMISSION;
		}

		@Override
		public void receiveResponse(INKFResponseReadOnly aResponse, INKFRequestContext aContext) throws Exception
		{
			int connections=mActiveConnections.decrementAndGet();
			//System.out.println("receiveResponse in state "+mState);
			if (mState==ConnectionState.CONNECTED || mState==ConnectionState.DISABLED)
			{	aContext.logRaw(INKFLocale.LEVEL_INFO, "Connection to Cluster Manager Lost");
			}
			if ((mState==ConnectionState.CONNECTING || mState==ConnectionState.CONNECTED) && connections==0)
			{	mState = ConnectionState.READY_TO_CONNECT;
			}
		}

		@Override
		public void receiveException(NKFException aException, INKFRequest aRequest, INKFRequestContext aContext) throws Exception
		{	//System.out.println("receiveException in state "+mState);
			int connections=mActiveConnections.decrementAndGet();
			if (mState==ConnectionState.CONNECTED || mState==ConnectionState.DISABLED)
			{	aContext.logRaw(INKFLocale.LEVEL_INFO, "Connection to Cluster Manager Lost");
			}
			if ((mState==ConnectionState.CONNECTING || mState==ConnectionState.CONNECTED) && connections==0)
			{	mState = ConnectionState.READY_TO_CONNECT;
			}
		}
		
	}

	@Override
	public void spaceChanged()
	{
		if (mState==ConnectionState.WAIT_FOR_REBOOT)
		{	mState=ConnectionState.READY_TO_CONNECT;
		}
		
	}
}
