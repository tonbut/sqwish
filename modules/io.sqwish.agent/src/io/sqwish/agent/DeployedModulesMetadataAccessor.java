package io.sqwish.agent;

import java.util.Set;

import org.netkernel.ext.system.representation.IRepDeployedModules;
import org.netkernel.layer0.meta.IEndpointMeta;
import org.netkernel.layer0.meta.IModuleMeta;
import org.netkernel.layer0.meta.ISpaceElements;
import org.netkernel.layer0.meta.ISpaceMeta;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.nkf.INKFRequestReadOnly;
import org.netkernel.layer0.nkf.INKFResponse;
import org.netkernel.layer0.nkf.INKFResponseReadOnly;
import org.netkernel.layer0.representation.IHDSNode;
import org.netkernel.layer0.urii.SimpleIdentifierImpl;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.builtin.PrivateFilterEndpoint;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.urii.IEndpoint;
import org.netkernel.urii.IIdentifier;
import org.netkernel.urii.IMetaRepresentation;
import org.netkernel.urii.ISpace;
import org.netkernel.urii.ISpaceWithIdentity;
import org.w3c.dom.Element;

public class DeployedModulesMetadataAccessor extends StandardAccessorImpl
{
	public DeployedModulesMetadataAccessor()
	{	this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		String namespace=aContext.source("arg:namespace",String.class);
		IRepDeployedModules moduleList=aContext.source("active:moduleList",IRepDeployedModules.class);
		IHDSReader replicaSets=SinkDeploymentAccessor.getInstance().getLastReplicaSets().getReader();
		
		//locate space
		ISpaceWithIdentity space=this.getStandardSpace().getOwningModule().getSpace(new SimpleIdentifierImpl("urn:io:sqwish:agent:dynamic"));
		String spaceHash=moduleList.hashForComponent(space);
		
		//issue meta request
		INKFRequest req=aContext.createRequest("");
		req.setVerb(INKFRequestReadOnly.VERB_META);
		req.injectRequestScope(space);
		req.setRepresentationClass(ISpaceMeta.class);
		INKFResponseReadOnly<ISpaceMeta> metaResponse=aContext.issueRequestForResponse(req);
		ISpaceMeta metaRep=metaResponse.getRepresentation();
		
		//serialize metadata
		ISpaceElements elements=metaRep.getElements();
		IHDSMutator m=HDSFactory.newDocument();
		m.pushNode("endpoints");
		for (int i=0; i<elements.size(); i++)
		{	IIdentifier id=elements.getIdentifier(i);
			try
			{	
				if (id!=null)
				{	
					String idString=id.toString();
					String elementHash=spaceHash+"/"+idString;
					String originHash=findOriginatingModule(elementHash, moduleList);
					IModuleMeta moduleMeta=(IModuleMeta)moduleList.metadataForHash(originHash);
					String namespaceId=getNamespaceForModule(moduleMeta,replicaSets);
					//System.out.println(idString+" -> "+moduleMeta.getName()+" "+namespaceId);
					
					if (namespaceId==null || !namespaceId.equals(namespace))
					{	continue;
					}
					
					req=aContext.createRequest(idString);
					req.setVerb(INKFRequestReadOnly.VERB_META);
					req.injectRequestScope(space);
					req.setRepresentationClass(IMetaRepresentation.class);
					INKFResponseReadOnly<IMetaRepresentation> epMetaResponse=aContext.issueRequestForResponse(req);
					IMetaRepresentation epMetaBase=epMetaResponse.getRepresentation();
					
					boolean isPublic= (epMetaBase.getAdditionalFields().getValue(PrivateFilterEndpoint.KEY_PRIVATE))==null;
					if (isPublic && epMetaBase instanceof IEndpointMeta)
					{	
						IEndpointMeta epMeta=(IEndpointMeta)epMetaBase;
						String grammarString=epMeta.getIdentifierGrammar().toXMLString();
						if(epMeta.getSupportedVerbs()!=INKFRequestReadOnly.VERB_TRANSREPRESENT && !grammarString.contains("SimpleDynamicImportHook.xml"))
						{	
							m.pushNode("endpoint")
							.addNode("id", id.toString())
							.addNode("name", epMeta.getName())
							.addNode("desc", epMeta.getDescription())
							.addNode("verbs", Integer.toString(epMeta.getSupportedVerbs()))
							.addNode("grammar",grammarString)
							.popNode();
						}
					}
				}
			}
			catch (Exception e)
			{	//ignore any duff endpoints
			}
		}
		aContext.createResponseFrom(m.toDocument(false));
		
	}
	
	private static String getNamespaceForModule(IModuleMeta aModuleMeta, IHDSReader aReplicaSets)
	{
		String uri=aModuleMeta.getIdentifier();
		String version=aModuleMeta.getVersion();
		String xpath=String.format("/replicaSets/replicaSet/module[uri='%s' and version='%s']/../@id",
				uri, version );
		String result=(String)aReplicaSets.getFirstValueOrNull(xpath);
		return result; 
		
	}
	
	
	private static String findOriginatingModule(String aElementHash, IRepDeployedModules aModules)
	{
		int j=aElementHash.lastIndexOf('/');
		String elementId=aElementHash.substring(j+1);
		String spaceHash=aElementHash.substring(0,j);
		String resultSpaceHash=spaceHash;
		String resultPhysicalHash=null;
		
		do
		{	ISpaceMeta spaceMeta=(ISpaceMeta)aModules.metadataForHash(spaceHash);
			spaceHash=null;
			ISpaceElements spaceElements=spaceMeta.getElements();
			for (int i=0; i<spaceElements.size(); i++)
			{	IIdentifier eid=spaceElements.getIdentifier(i);
				if (eid!=null && eid.toString().equals(elementId))
				{	IEndpoint physicalEp=spaceElements.getPhysicalEndpoint(i);
					resultPhysicalHash=aModules.hashForComponent(physicalEp);
					ISpace delegatedSpace=spaceElements.getDelegatedSpace(i);
					if (delegatedSpace!=null)
					{	resultSpaceHash=aModules.hashForComponent(delegatedSpace);
						spaceHash=resultSpaceHash;
					}
					else
					{	break;
					}
				}
			}
		} while(spaceHash!=null);
		int i=resultSpaceHash.indexOf('/');
		return resultSpaceHash.substring(0,i);
	}
	
}
