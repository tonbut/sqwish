package io.sqwish.agent;

import java.net.URI;

import org.netkernel.container.config.IConfiguration;
import org.netkernel.layer0.boot.BootUtils;
import org.netkernel.layer0.nkf.INKFLocale;
import org.netkernel.layer0.nkf.INKFRequest;
import org.netkernel.layer0.nkf.INKFRequestContext;
import org.netkernel.layer0.representation.IBinaryStreamRepresentation;
import org.netkernel.mod.hds.HDSFactory;
import org.netkernel.mod.hds.IHDSDocument;
import org.netkernel.mod.hds.IHDSMutator;
import org.netkernel.mod.hds.IHDSReader;
import org.netkernel.module.standard.endpoint.StandardAccessorImpl;
import org.netkernel.util.Utils;

import io.sqwish.agent.ServerConnectionTransport.ConnectionState;

public class SinkDeploymentAccessor extends StandardAccessorImpl
{
	private IHDSDocument mLastReplicaSets;
	private static SinkDeploymentAccessor sInstance;
	
	static SinkDeploymentAccessor getInstance()
	{	return sInstance;
	}
	
	IHDSDocument getLastReplicaSets()
	{	return mLastReplicaSets;
	}
	
	public SinkDeploymentAccessor()
	{	sInstance=this;
		this.declareThreadSafe();
	}
	
	public void onSource(INKFRequestContext aContext) throws Exception
	{
		try
		{
			ServerConnectionTransport.getInstance().serverCallbackReceived();
			
			if (ServerConnectionTransport.getInstance().getConnectionState()!=ConnectionState.CONNECTED)
			{	return;
			}
			
			IHDSReader replicaSets=aContext.source("arg:operand",IHDSDocument.class).getReader();
			
			//ensure we have local copy of any modules
			boolean newDownloads=false;
			for (IHDSReader module : replicaSets.getNodes("/replicaSets/replicaSet/module"))
			{	newDownloads|=ensureLocalCopyOfModule(module,aContext);
			}
			
			//compare with existing deployment state
			IHDSMutator m=HDSFactory.newDocument();
			m.pushNode("modules");
			m.addNode("@devmode", "true");
			for (IHDSReader module : replicaSets.getNodes("/replicaSets/replicaSet/module"))
			{
				String hash=(String)module.getFirstValue("content_md5");
				String destFileURI=getDestFileURIForHash(hash,aContext);
				m.pushNode("module").addNode("src", destFileURI).popNode();
			}
			INKFRequest req=aContext.createRequest("active:synchronizeDeployedModules");
			req.addArgumentByValue("operand", m.toDocument(false));
			req.addArgumentByValue("force", newDownloads);
			req.setRepresentationClass(Boolean.class);
			Boolean changed=(Boolean)aContext.issueRequest(req);
			
			mLastReplicaSets=replicaSets.toDocument();
			
			if (changed)
			{	
				ServerConnectionTransport.getInstance().disconnectAndWaitForReboot();
			}
			
			//if changed then update
			String msg="Synchronize deployed modules received from cluster manager, changes="+changed;
			aContext.logRaw(INKFLocale.LEVEL_INFO, msg);
		
		}
		catch (Throwable e)
		{
			System.out.println(Utils.throwableToString(e));
		}
		
	}
	
	private boolean ensureLocalCopyOfModule(IHDSReader aModule, INKFRequestContext aContext) throws Exception
	{
		String hash=(String)aModule.getFirstValue("id");
		String remoteMD5=(String)aModule.getFirstValue("content_md5");
		
		String destFileURI=getDestFileURIForHash(remoteMD5,aContext);
		boolean needToDownload=false;
		if (!aContext.exists(destFileURI))
		{	needToDownload=true;
		}
		else
		{	
			INKFRequest req = aContext.createRequest("active:md5");
			req.addArgument("operand", destFileURI);
			req.setRepresentationClass(String.class);
			String localMD5=(String)aContext.issueRequest(req);
			
			if (!localMD5.equals(remoteMD5))
			{	needToDownload=true;
			}
		}
		
		if (needToDownload)
		{
			String moduleURI=(String)aModule.getFirstValue("uri");
			String version=(String)aModule.getFirstValue("version");
			String msg="Downloading new module "+moduleURI+" "+version;
			aContext.logRaw(INKFLocale.LEVEL_INFO, msg);
			INKFRequest req=aContext.createRequest("active:downloadModule");
			req.addArgument("hash", hash);
			req.setRepresentationClass(IBinaryStreamRepresentation.class);
			IBinaryStreamRepresentation moduleData=(IBinaryStreamRepresentation)aContext.issueRequest(req);
			//System.out.println("sinking to "+destFileURI);
			aContext.sink(destFileURI, moduleData);
		}
		
		return needToDownload;
	}
	
	private static String getDestFileURIForHash(String aHash, INKFRequestContext aContext)
	{
		IConfiguration config = aContext.getKernelContext().getKernel().getConfiguration();
		URI base = URI.create(BootUtils.getInstallPath(config));
		URI destFileURI = base.resolve(URI.create("sqwishy/"+aHash+".jar"));
		return destFileURI.toString();
	}

	
}
