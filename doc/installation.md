# Installation Guide

Sqwish requires certain pre-requisites on all compute instance on which clients, agents, and the cluster manager run. 

1. A linux virtual machine with one or more cores and at least 1Gb of RAM.
2. Install Java. Either Oracle or OpenJDK will do 1.8 upwards.
3. Install NetKernel Enterprise Edition - after installation use the Apposite tools to get latest updates.

## Cluster Manager Install
1. Follow the above foundation steps.
2. Using Apposite tool in NetKernel install html5-frameworks, web-framework modules in NetKernel.
3. Install the sqwish cluster manager module into NetKernel. (Use the upload module tool and select the jar file from `build/lib` folder)
4. Copy the `test/configuration_files/sqwishConfig.xml` file into the NetKernel installation directory subfolder of `etc/`
5. Edit configuration file - see [Configuration Guide](configuration.md).
6. Start NetKernel, open a terminal in the root of the NetKernel installation directory and run the command `bin/netkernel.sh`.

##Agent Install
Each agent must be set up as follows:
1. Follow the above foundation steps.
2. Install the sqwish agent module into NetKernel. (Use the upload module tool and select the jar file from `build/lib` folder)
3. Copy the `test/configuration_files/sqwishAgentConfig.xml` file into the NetKernel installation directory subfolder of `etc/`
4. Edit configuration file - see [Configuration Guide](configuration.md).
5. Start NetKernel, open a terminal in the root of the NetKernel installation directory and run the command `bin/netkernel.sh`.
