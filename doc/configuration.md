# Configuration

## Cluster Manager Configuration

A `sqwishConfig.xml` file is create in the [netkernel install]/etc folder. An example is found in the repo at `/test/configuration_files/sqwishConfig.xml`
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sqwish>
    <agentTimeout>1000</agentTimeout>
    <replicaSet id="default">
        <module src="file:/Users/tab/dev/projects/Sqwish/TestModules/b.jar"/>
        <module src="file:/Users/tab/dev/projects/Sqwish/TestModules/c.jar"/>
    </replicaSet>
    <users>
        <user>
            <name>tab</name>
            <password>....</password>
        </user>
    </users>
    <tls>
        <keystore>file:/Users/tab/dev/projects/Sqwish/mysslstore.jks</keystore>
        <keyStorePassword>...</keyStorePassword>
        <keyPassword>...</keyPassword>
    </tls>
</sqwish>
```

* **agentTimeout** - number of milliseconds that is allowed for agents to respond to control plane requests such as telemetry, notification of updates, and requests for namespace metadata
* **replicaSet** - (to be renamed to namespace) - one or more namespace definitions to deploy onto available agents
    * **id** - globally unique identifier for each namespace - clients will use this to access functionality
    * **module** - a fully qualified URI to the source jar of a NetKernel module to be used in this namespace
* **users** - a set of one or more authentication credentials that clients and agents can use to connect to the cluster manager
    * **name** - a string username
    * **password** - the salted password hash for the user. See below for how to create.
* **tls** - the transport layer security configuration - optional - without tls the cluster manager will use plain sockets
    * **keystore** - a fully qualified URI to a java keystore containing a single certificate. See below for how to create a self signed certificate.
    * **keyStorePassword** - as specified when keystore is created.
    * **keyPassword** - as specified when keystore is created.

## Password Hash
To generate a password hash you must currently issue a request using the request trace tool within the NetKernel control panel.
![Generate Password Hash](img/generatePasswordHash.png "Generate Password Hash")

## Creating a self-signed keystore
See https://medium.com/@maanadev/netty-with-https-tls-9bf699e07f01

```
keytool  -genkey -noprompt -trustcacerts -keyalg RSA -alias cert -dname  maanadev.org -keypass 123456 -keystore mysslstore.jks -storepass 123456 -dname CN=maanadev.org
```

## Agent Configuration

A `sqwishAgentConfig.xml` file is create in the [netkernel install]/etc folder. An example is found in the repo at `/test/configuration_files/sqwishAgentConfig.xml`
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sqwish>
    <host>localhost</host>
    <port>32768</port>
    <username>tab</username>
    <password>abc</password>
    <tls>explicitTrust</tls>
</sqwish>
```

* **host** - hostname or IP address of cluster manager
* **port** - port on cluster manager - currently must be 32768
* **username** - authentication username as registered in cluster manager configuration
* **password** - authentication password
* **tls** - tri state - false to use plain sockets, true to use TLS encryption, explicitTrust to use TLS encryption but don't verify certificate which is useful for self-signed certificates.

## Client Configuration
The client configuration file is currently identical in structure to the agent configuration. An example is found in the repo at `/test/configuration_files/sqwishClientConfig.xml`