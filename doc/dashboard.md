##Dashboard
The dashboard is accessed from the netkernel control panel on port 1060 of the cluster manager:
![Extras Panel](img/extrasPanel.png "Extras Panel")

![Dashboard](img/dashboard.png "Dashboard")

Buttons at the top enable and disable ingress and egress of requests from the cluster manager. In addition, when ingress and egress are disabled, pending requests can be flushed.

Clicking on an agent within the Agents table drills down to more detail on the state of an agent.