# Roadmap / Limitations
* Management API to expose operating parameters and control
* Advanced routing capabilities such as  A/B testing
* Circuit breaker functionality
* Management API for integration with 3rd party monitoring tools
* Cluster manager persistent storage and failover. 
* Every agent gets assigned every namespace
* Every agent gets assigned `C+2` concurrent requests maximum where C is the number of cores that that agent has.
* Support for non-NetKernel clients and agents using REST API.
