# Running examples
Currently there is a single example setup that contains a number of endpoints to trigger workloads.

* Setup a client NetKernel instance and deploy client module and `test/io.sqwish.client.test/` module.
* Setup cluster manager with a namespace named default containing the `test/urn.io.sqwish.test.c/` module
* Setup an agent NetKernel instance
* Start cluster manager, client, and agent instances
* Using the request trace tool on client instance you can issue the following requests into the `urn:sqwish:client:test` module:
    * **res:/modulec/java** - issues the single request direct to cluster - this endpoint contains a tight loop that consumes a single core CPU on an agent roughly 1/5 of a second
    * **res:/modulec/sleep** - issues the single request direct to cluster - this endpoint sleeps for 1 second before returning "done"
    * **res:/modulec/passthrough** -  issues the single request direct to cluster - this endpoint issues a sub request back to the cluster for `res:/modulec/java`
    * **fib(4)** -  issues the single request direct to cluster - calls an endpoint that implements a simple recursive fibonacci algorithm. Each recursive sub step is issue back to the cluster which results in a tree of sub-requests. (There are currently issues with running larger numbers)
    * **res:/test/throughput** - runs test harness for 100 seconds which issues as many requests as it can with a concurrency of 32 and displays the throughput. The request issued is to res:/modulec/java
    * **res:/test/dataset** - runs a test harness for 120 seconds with a varying load by delaying between 2ms and 37ms between issuing a new request. Requests are issued to `res:/modulec/caching/[random number]` where random number is between 0 and 499. The response on this endpoint can be cached for 100ms 
    * **res:/test/blast** - issues 500 concurrent requests instantaneously to `res:/modulec/java` and waits for all responses.
