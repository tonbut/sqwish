#Development Guide

##Developing Agent functionality
Developing agent functionality means developing the functionality that can be highly scaled. Sometimes these can be as simple as discrete parcels of work, other times these might be services that might depend upon other services or databases.

* Namespaces are self-contained in that they cannot reference other namespaces functionality or assume that another namespaces is deployed on the same agent.
* Namespaces can instantiate a client and either recursively issue requests to it's own namespace or another.
* Namespaces define an interface as a NetKernel space that is dynamically imported into agent.
* One or modules in a namespace must declare a NetKernel dynamic import hook so that endpoints can be exposed. This allows the endpoints to be discovered:
```xml
<literal type="xml" uri="res:/etc/system/SimpleDynamicImportHook.xml">
    <connection>
    <type>Sqwish</type>
    </connection>
</literal>
```

See the module `/module/urn.io.sqwish.test.c/` as an example.

##Developing Client functionality
Currently clients must be NetKernel modules. The module must import the urn:io:sqwish:client module, then declare an instance of the SqwishClient prototype:
```xml
<import>
        <uri>urn:io:sqwish:client</uri>
</import>
<endpoint>
        <prototype>SqwishClient</prototype>
        <namespace>default</namespace>
</endpoint>
```
This acts as a gateway to all the endpoints with a namespace via the cluster manager. Requests can then be issued which will resolve to the SqwishClient and be routed to the cluster manager. 

See the `test/io.sqwish.client.test/` module as an example.
