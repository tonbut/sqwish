# Resource Reference
## Cluster Manager Internal Resources
These resources and their representations are documented here for developmental purposes and are not useful for developers of functionality on Sqwish

### active:agentList
The list of agents currently connected.
```xml
  agents: 
    agent: 
      id: wollemi
      ip: 192.168.1.135
      connected: true
      lastConnect: 1588265959827
      context: NKFContextImpl[ ]
```

### active:agentAssignments
How the namespaces are assigned to agents.
```xml
  assignments: 
    namespaces: 
      namespace: 
        @id: default
        agent: wollemi
      namespace: 
        @id: second
        agent: wollemi
    agents: 
      agent: 
        @id: wollemi
        namespace: default
        namespace: second
```

### active:routingTable
State needed to route requests within each namespace.
```xml
  namespaces: 
    namespace: 
      id: default
      agent: 
        id: wollemi
        context: NKFContextImpl[ ]
        lastConnect: 1588265959827
        cores: 4
    namespace: 
      id: second
      agent: 
        id: wollemi
        context: NKFContextImpl[ ]
        lastConnect: 1588265959827
        cores: 4
```

### active:cmInfo
The current telemetry for the cluster manager.
```xml
  info: 
    heap: 
      baseline: 0
      peak: 536898
      alloc: 1398272
    cache: 
      size: 6971
      peakSize: 9999
      troughSize: 7009
      hitRate: 25.11
    requests: 
      requestRate: 6
      activeRootRequests: 1.12
    threads: 
      total: 63
      poolSize: 16
      poolBusy: 0
    cpu: 
      userCPU: 0
      gcCPU: 0
      processors: 12
      corePerformance: 3.40
```

### active:routeClientRequestStatus
The live status of the request router within the cluster manager.
```xml
  clientRequestRouter: 
    agents: 
      agent: 
        id: wollemi
        concurrency: 0
        executed: 2
        executedRate: 0.0
    namespaces: 
      namespace: 
        id: default
        queue: 0
        accepted: 2
        executed: 2
        completed: 2
        queueWait: 0
        endpoints: 
          endpoint: 
            id: test:c:java
            executed: 1
            responseTime: 424
          endpoint: 
            id: test:c:passthrough
            executed: 1
            responseTime: 454
    clients: 
      client: 
        id: /192.168.1.135:42078
        namespace: default
        received: 1
        receivedRate: 0.22446689
      client: 
        id: /127.0.0.1:64198
        namespace: default
        received: 0
        receivedRate: 0.0
      client: 
        id: /127.0.0.1:64204
        namespace: default
        received: 1
        receivedRate: 0.22446689
```

### active:cmState
The operational state of the cluster manager.
```xml
  state: 
    ingress: true
    egress: true
```

### active:stateAggregation
```xml
  state: 
    clients: 
      client: 
        id: /127.0.0.1:52283
        namespace: default
        received: 0
        receivedRate: 0.0
      client: 
        id: /127.0.0.1:52290
        namespace: default
        received: 7824
        receivedRate: 0.0
      client: 
        id: /192.168.1.135:53554
        namespace: default
        received: 1
        receivedRate: 0.0
    namespaces: 
      namespace: 
        availableAgents: 1
        id: default
        queue: 0
        accepted: 7825
        executed: 7825
        completed: 7825
        queueWait: 0
        endpoints: 
          endpoint: 
            id: test:c:java
            responseTime: 282
            executed: 2481
          endpoint: 
            id: test:c:caching
            responseTime: 148
            executed: 5343
          endpoint: 
            id: test:c:longsleep
            executed: 0
          endpoint: 
            id: test:c:fibonacci
            executed: 0
          endpoint: 
            id: test:c:groovy2
            executed: 0
          endpoint: 
            id: test:c:reboot
            executed: 0
          endpoint: 
            id: test:c:passthrough
            responseTime: 878
            executed: 1
      namespace: 
        availableAgents: 1
        id: second
        queue: 0
        queueWait: 0
        accepted: 0
        executed: 0
        completed: 0
        endpoints: 
          endpoint: 
            id: test:a:groovy
            executed: 0
    agents: 
      agent: 
        id: wollemi
        ip: 192.168.1.135
        connected: true
        correct: true
        id: wollemi
        concurrency: 0
        executed: 7825
        executedRate: 0.0
        heap: 
          baseline: 13251
          peak: 45505
          alloc: 133888
        cache: 
          size: 3715
          peakSize: 7933
          troughSize: 5906
          hitRate: 3.17
        requests: 
          requestRate: 1
          activeRootRequests: 1.02
        threads: 
          total: 37
          poolSize: 8
          poolBusy: 0
        cpu: 
          userCPU: 0
          gcCPU: 0
          processors: 4
          corePerformance: 0.58
      count: 1
      countGood: 1
      countBad: 0
      utilisation: 0.0
      totalComputePower: 2.32
    cm: 
      heap: 
        baseline: 0
        peak: 551926
        alloc: 1398272
      cache: 
        size: 7277
        peakSize: 9999
        troughSize: 7009
        hitRate: 24.29
      requests: 
        requestRate: 5
        activeRootRequests: 1.02
      threads: 
        total: 63
        poolSize: 16
        poolBusy: 0
      cpu: 
        userCPU: 0
        gcCPU: 0
        processors: 12
        corePerformance: 3.40
```